# mist-tmtc
The iOBC telecommands &amp; telemetry data handling implementation.

### Main Feature
Provide an intuitive API to be used for retrieving telecommands and sending telemetries from and to groundstation. It hides the low level implementation in generating packets as well as the actual implementation in communicating with both simulator or real TRXVU is used.

The `application` namespace is used for implementations of MIST-specific protocols on top of CCSDS, such as the telemetry fragmentation protocol.

### Run the example project
1. From Eclipse ISIS IDE, import Existing Projects into Workspace, and point to `example` folder.
2. Make sure the project refers to the correct location of ISIS SDK. It may be different in different computer, depends on where does the user put the ISIS SDK references.
3. Check and modify the configuration in `tmtc-api/tmtc_config.h` based on needs. The only matters is whether the simulator or real TRXVU is used. Open the doxygen documentation for more information.
4. Finally, Compile and Run Debug.

### Import the API into another project
1. Copy the `tmtc-api` sub folder into your project.
2. Add the newly copied `tmtc-api` sub folder into the project's include path.

### Add as a Git Submodule
1. `git submodule add git@gitlab.com:kth-mist/tmtc-api.git`.
2. `git submodule update --init --recursive`.
3. Add the new `tmtc-api` sub folder into the project's include path.
4. Right click the `tmtc-api` sub folder (same as point 3), select `properties` and uncheck `Exclude resource from build`.

### Using the API
1. Include the `<tmtc-api.h>` into your source file.
2. Check and modify the configuration as needed in `tmtc-api/src/tmtc_config.h`.
3. Implement the callback functions in `tmtc-api/tmtc_callbacks.h` which handle packet sequence counters.
   Below are examples which should be replaced with ones that read from non-volatile memory available in the OBC (NORFlash or FRAM):
```c
uint16_t tmtc_ccsds_sequence_count_cb(uint16_t apid)
{
    static uint16_t ccsds_psc_counter_apid_eps  = 0x00;
    static uint16_t ccsds_psc_counter_apid_com  = 0x00;
    static uint16_t ccsds_psc_counter_apid_cdms = 0x00;
    static uint16_t ccsds_psc_counter_apid_adcs = 0x00;
    static uint16_t ccsds_psc_counter_apid_pl   = 0x00;
    static uint16_t ccsds_psc_counter_apid_beac = 0x00;

    switch (apid) {
        case TMTC_APID_EPS  : return ccsds_psc_counter_apid_eps++;
        case TMTC_APID_COM  : return ccsds_psc_counter_apid_com++;
        case TMTC_APID_CDMS : return ccsds_psc_counter_apid_cdms++;
        case TMTC_APID_ADCS : return ccsds_psc_counter_apid_adcs++;
        case TMTC_APID_PL   : return ccsds_psc_counter_apid_pl++;
        case TMTC_APID_BEAC : return ccsds_psc_counter_apid_beac++;
        default: return 0;
    }
}
```
```c
uint8_t tmtc_ax25_master_frame_count_cb(void)
{
    static uint8_t ax25_master_frame_counter = 0x00;
    return ax25_master_frame_counter++;
}
```
```c
uint8_t tmtc_ax25_virtual_frame_count_cb(uint8_t virtual_id)
{
    static uint8_t counters[8];

    if (virtual_channel_id > 7)
        return 0;

    return counters[virtual_channel_id]++;
}
```

### Initialize the API
The API needs to be initialized by calling [`tmtc_setup()`](tmtc-api/src/tmtc.c#L28).

### Example
1. Sending a telemetry:
```c
int err;
uint8_t data_length = 4;
uint8_t data_to_be_Sent[4];
struct tmtc_telemetry tm;
struct tmtc_tm_params params;

data_to_be_Sent[0] = 'M';
data_to_be_Sent[1] = 'I';
data_to_be_Sent[2] = 'S';
data_to_be_Sent[3] = 'T';

params.virtual_channel_id = 0;
params.apid = 12;
params.service_type = 176;
params.service_subtype = 13;
params.time = 0x00000000;
params.payload = data_to_be_Sent;
params.payloadlen = data_length;

err = tmtc_generate_tm(&tm, &params);
if (err != 0) {
    // Handle error
}

err = tmtc_send_tm(&tm);
if (err != 0) {
    // Handle error
}
```
2. Retrieving a telecommand:
```c
struct tmtc_telecommand tc;

err = tmtc_get_tc(&tc);
if (err != 0) {
    // Handle error
}
```
3. Acknowledge a telecommand
```c
struct tmtc_telemetry ack;
struct tmtc_ack_params params;

if (tc.ccsds.ack & 8) { // acceptance acknowledgement flag set in telecommand
    params.virtual_channel_id = 0;
    params.ack_subtype = TMTC_ST_ACK_SST_ACCEPTANCE_SUCCESS;
    params.time = 0x00000000;
    params.tc_apid = tc.ccsds.apid;
    params.tc_sequence_count = tc.ccsds.sequence_count;
    params.ack_error_code = TMTC_ACK_ERROR_OK;
}
```
