SRC_DIR = example/src \
          tmtc-api/src

OBJ_DIR = $(patsubst %,BUILD/%,$(SRC_DIR))

SRC_FILES = $(foreach dir,$(SRC_DIR),$(shell find $(dir) -type f -name "*.c"))

OBJ_FILES = $(patsubst %.c,%.o,$(SRC_FILES))

D_FILES = $(patsubst %.c,%.d,$(SRC_FILES))

C_FLAGS = -mcpu=arm926ej-s \
		  -O0 \
		  -fmessage-length=0 \
		  -ffunction-sections \
		  -Wall \
		  -Wextra \
		  -g \
		  -Dsdram \
		  -Dat91sam9g20 \
		  -DTRACE_LEVEL=5 \
		  -DDEBUG=1 \
		  -D'BASE_REVISION_NUMBER=' \
		  -D'BASE_REVISION_HASH_SHORT=' \
		  -D'BASE_REVISION_HASH=' \
		  -std=c99 \
		  -MMD \
		  -MP

INCDIR_FLAGS = -I"tmtc-api/include" \
               -I"isis-sdk/hal/freertos/include" \
               -I"isis-sdk/hal/hal/include" \
               -I"isis-sdk/hal/hcc/include" \
               -I"isis-sdk/hal/at91/include" \
               -I"isis-sdk/mission-support/include" \
               -I"isis-sdk/satellite-subsystems/include"

LINKER_SCRIPT = "isis-sdk/hal/at91/linker-scripts/sdram.lds"

LINKER_FLAGS = -mcpu=arm926ej-s \
               -O0 -fmessage-length=0 \
               -ffunction-sections \
               -Wall \
               -Wextra \
               -g \
               -T $(LINKER_SCRIPT) \
               -nostartfiles \
               -Xlinker \
               --gc-sections \
               -Wl,-Map,"tmtc-example.map" \
               --specs=nano.specs \
               -lc \
               -u _printf_float \
               -u _scanf_float

LD_FLAGS = -lMissionSupportD \
           -lSatelliteSubsystemsD \
           -lHCCD \
           -lHALD \
           -lFreeRTOSD \
           -lAt91D

LIBDIR_FLAGS = -L"isis-sdk/hal/freertos/lib" \
               -L"isis-sdk/hal/hal/lib" \
               -L"isis-sdk/hal/hcc/lib" \
               -L"isis-sdk/hal/at91/lib" \
               -L"isis-sdk/mission-support/lib" \
               -L"isis-sdk/satellite-subsystems/lib"

CC = arm-none-eabi-gcc
COPY = arm-none-eabi-objcopy
SIZE = arm-none-eabi-size

GCC = gcc

all: clone-dependency tmtc-example.elf

clone-dependency:
	if [ ! -d "isis-sdk" ]; then git clone git@gitlab.com:kth-mist/isis-sdk.git; fi

tmtc-example.elf: $(OBJ_FILES)
	$(CC) -o $@ $^ $(LINKER_FLAGS) $(LIBDIR_FLAGS) $(LD_FLAGS)
	$(COPY) -O binary $@  $(patsubst %.elf,%.bin,$@)
	$(SIZE) --format=berkeley $@

%.o: %.c
ifeq ($(MAKECMDGOALS),test)
	$(GCC) -c -o $@ $< $(TEST_INCDIR_FLAGS)
else
	$(CC) -c -o $@ $< -MF$(patsubst %.o,%.d,$@) -MT$(patsubst %.o,%.d,$@) $(INCDIR_FLAGS) $(C_FLAGS)
endif

.PHONY = clean clean-test clean-test-mingw clean-test-linux

clean:
	rm $(OBJ_FILES)
	rm $(D_FILES)
	rm tmtc-example.elf
	rm tmtc-example.map
	rm tmtc-example.bin

TEST_DIR = test tmtc-api/src/application
TEST_SRC_FILES= tmtc-api/src/ax25.c tmtc-api/src/ccsds.c tmtc-api/src/crc.c tmtc-api/src/tmtc.c
TEST_SRC_FILES+= $(foreach dir,$(TEST_DIR),$(shell find $(dir) -type f -name "*.c"))
TEST_OBJ_FILES = $(patsubst %.c,%.o,$(TEST_SRC_FILES))
TEST_INCDIR_FLAGS = -I"munit" -I"tmtc-api/include"

test: compile-test
	./test-runner --color always

compile-test: munit/munit.o $(TEST_OBJ_FILES)
	$(GCC) -o test-runner $^

clean-test:
	rm $(TEST_OBJ_FILES)
	rm munit/munit.o
	rm test-runner
