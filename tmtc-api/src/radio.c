/**
 * @file radio.c
 * @author Muhamad Andung Muntaha <muam@kth.se>
 * @author John Wikman
 * @author Erik Flink
 * @author Theodor Stana
 *
 * @brief Radio related function implementation
 */

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <hal/errors.h>
#include <satellite-subsystems/IsisTRXVU.h>

#include <tmtc-api/error.h>
#include <tmtc-api/radio.h>

#include <tmtc-api/config/config.h>

static int _chosen_radio_index;

static uint8_t _tmtcvar_last_avail;

static int tmtc_radio_to_7_char_callsign(uint8_t *callsign, uint8_t ssid, uint8_t *dst);

/**
 * @brief Initializes the radio module in the TMTC API.
 *
 * @param[in] sim An optional radio simulator. If not NULL, The simulator will
 *                be probed using an I2C command and used instead of the real
 *                radio if it replies
 *
 * @return 0 on success. Otherwise an error code from error.h.
 */
int tmtc_radio_initialize(struct tmtc_radio *sim)
{
	int err, i, count;
	ISIStrxvuI2CAddress i2c_addrs[2];
	ISIStrxvuFrameLengths framelens[2];
	ISIStrxvuBitrate bitrates[2];

	/* Determine TRXVU count and I2C addresses */
	i2c_addrs[TMTC_CONFIG_TRXVU_INDEX].addressVu_rc = TMTC_CONFIG_TRXVU_RX_I2C_ADDRESS;
	i2c_addrs[TMTC_CONFIG_TRXVU_INDEX].addressVu_tc = TMTC_CONFIG_TRXVU_TX_I2C_ADDRESS;

	if (sim != NULL) {
		i2c_addrs[TMTC_CONFIG_TRXVU_SIM_INDEX].addressVu_rc = sim->i2c_rx_addr;
		i2c_addrs[TMTC_CONFIG_TRXVU_SIM_INDEX].addressVu_tc = sim->i2c_tx_addr;

		count = 2;
	} else {
		count = 1;
	}

	/* Determine shared TRXVU configuration */
	for (i = 0; i < count; i++) {
		framelens[i].maxAX25frameLengthRX = TMTC_TRXVU_UPLINK_PAYLOAD_MAXSIZE;
		framelens[i].maxAX25frameLengthTX = TMTC_TRXVU_DOWNLINK_PAYLOAD_MAXSIZE;

		switch (TMTC_CONFIG_TRXVU_BITRATE) {
		case 1200:
			bitrates[i] = trxvu_bitrate_1200;
			break;
		case 2400:
			bitrates[i] = trxvu_bitrate_2400;
			break;
		case 4800:
			bitrates[i] = trxvu_bitrate_4800;
			break;
		case 9600:
			bitrates[i] = trxvu_bitrate_9600;
			break;
		default:
			bitrates[i] = trxvu_bitrate_1200;
			break;
		}
	}

	/* Initialize functional parameters */
	err = IsisTrxvu_initialize(i2c_addrs, framelens, bitrates, count);
	if (err != E_NO_SS_ERR)
		return TMTC_ERR_RADIO_TRXVU_INITIALIZE_FAILED;

	_tmtcvar_last_avail = 0;

	/* Probe to figure out which radio to use for TC/TM, prioritize simulator if available */
	for (_chosen_radio_index = count - 1; _chosen_radio_index >= 0; _chosen_radio_index--) {
		/* Initialize callsigns */
		err = tmtc_radio_set_to_callsign((uint8_t *) TMTC_CONFIG_AX25_TO_CALLSIGN, TMTC_CONFIG_AX25_TO_SSID);
		if (err != 0)
			continue;

		err = tmtc_radio_set_from_callsign((uint8_t *) TMTC_CONFIG_AX25_FROM_CALLSIGN, TMTC_CONFIG_AX25_FROM_SSID);
		if (err != 0)
			continue;

		return 0;
	}

	/* If we failed to set the callsign for both the simulator and real radios,
	   return a warning to notify the caller that initialization was successful but
	   that radio contact should be retried */
	if (err == TMTC_ERR_RADIO_COULD_NOT_SET_FROM_CALLSIGN || err == TMTC_ERR_RADIO_COULD_NOT_SET_TO_CALLSIGN)
		return TMTC_WARN_UNREACHABLE;

	return err;
}

/**
 * @brief Returns the index of the radio among that ones provided during
 *        initialization that is in use. If no simulator was given, this
 *        value is always zero.
 *
 * @note Assumes that the radio has been intialized
 *
 * @return The index, either TMTC_CONFIG_TRXVU_INDEX or TMTC_CONFIG_TRXVU_SIM_INDEX
 */
int tmtc_radio_index(void)
{
	return _chosen_radio_index;
}

/**
 * @brief Performs a soft reset on both the RX and TX radio modules.
 *
 * @return 0 on success. Otherwise an error code from error.h.
 */
int tmtc_radio_soft_reset(void)
{
	int err;

	err = IsisTrxvu_componentSoftReset(_chosen_radio_index, trxvu_rc);
	if (err != E_NO_SS_ERR)
		return TMTC_ERR_RADIO_FAILED_SOFT_RX_RESET;

	err = IsisTrxvu_componentSoftReset(_chosen_radio_index, trxvu_tc);
	if (err != E_NO_SS_ERR)
		return TMTC_ERR_RADIO_FAILED_SOFT_TX_RESET;

	return 0;
}

/**
 * @brief Performs a hard reset on both the RX and TX radio modules.
 *
 * @return 0 on success. Otherwise an error code from error.h.
 */
int tmtc_radio_hard_reset(void)
{
	int err;

	err = IsisTrxvu_hardReset(_chosen_radio_index);
	if (err != E_NO_SS_ERR)
		return TMTC_ERR_RADIO_FAILED_HARD_RESET;

	return 0;
}

/**
 * @brief Sets the radio tranmitter beacon mode.
 *
 * @param[in] src Data to transmit.
 * @param[in] len Length of data pointed to by src.
 * @param[in] interval Interval between beacon transmissions (seconds).
 *
 * @return 0 on success. Otherwise an error code from error.h.
 */
int tmtc_radio_set_beacon(uint8_t *src, size_t len, uint16_t interval)
{
	int err;
	unsigned char txlen;

	if (src == NULL)
		return TMTC_ERR_NULL_ARGUMENT;

	if (len > TMTC_TRXVU_DOWNLINK_PAYLOAD_MAXSIZE)
		return TMTC_ERR_RADIO_TX_DATA_TOO_LARGE;

	if (len == 0)
		return TMTC_ERR_RADIO_TX_DATA_TOO_SMALL;

	if (interval > TMTC_TRXVU_BEACON_MAX_INTERVAL)
		return TMTC_ERR_RADIO_TX_INVALID_INTERVAL;

	txlen = (unsigned char) (len & 0xff);

	err = IsisTrxvu_tcSetAx25BeaconDefClSign(_chosen_radio_index,
	                                         (unsigned char *) src,
	                                         txlen,
	                                         (unsigned short) interval);
	if (err != E_NO_SS_ERR)
		return TMTC_ERR_RADIO_FAILED_SET_BEACON;

	return 0;
}

/**
 * @brief Clears the radio tranmitter beacon mode.
 *
 * @return 0 on success. Otherwise an error code from error.h.
 */
int tmtc_radio_clear_beacon(void)
{
	int err;

	err = IsisTrxvu_tcClearBeacon(_chosen_radio_index);
	if (err != E_NO_SS_ERR)
		return TMTC_ERR_RADIO_FAILED_CLEAR_BEACON;

	return 0;
}

/**
 * @brief Sets the radio transmitter AX.25 TO callsign.
 *
 * @param[in] callsign Callsign to set (6 ASCII characters).
 * @param[in] ssid Secondary Station Identifier to set (between 0 and 15).
 *
 * @return 0 on success. Otherwise an error code from error.h.
 */
int tmtc_radio_set_to_callsign(uint8_t *callsign, uint8_t ssid)
{
	int err;
	unsigned char to_callsign[8];

	if (callsign == NULL)
		return TMTC_ERR_NULL_ARGUMENT;

	err = tmtc_radio_to_7_char_callsign(callsign, ssid, to_callsign);
	if (err != 0)
		return err;

	err = IsisTrxvu_tcSetDefToClSign(_chosen_radio_index, to_callsign);
	if (err != E_NO_SS_ERR)
		return TMTC_ERR_RADIO_COULD_NOT_SET_TO_CALLSIGN;

	return 0;
}

/**
 * @brief Sets the radio tranmitter AX.25 FROM callsign.
 *
 * @param[in] callsign Callsign to set (6 ASCII characters).
 * @param[in] ssid Secondary Station Identifier to set (between 0 and 15).
 *
 * @return 0 on success. Otherwise an error code from error.h.
 */
int tmtc_radio_set_from_callsign(uint8_t *callsign, uint8_t ssid)
{
	int err;
	unsigned char from_callsign[8];

	if (callsign == NULL)
		return TMTC_ERR_NULL_ARGUMENT;

	err = tmtc_radio_to_7_char_callsign(callsign, ssid, from_callsign);
	if (err != 0)
		return err;

	err = IsisTrxvu_tcSetDefFromClSign(_chosen_radio_index, from_callsign);
	if (err != E_NO_SS_ERR)
		return TMTC_ERR_RADIO_COULD_NOT_SET_FROM_CALLSIGN;

	return 0;
}

/**
 * @brief Sets the radio tranmitter idle state.
 *
 * @param[in] idle_state Idle state to set (boolean value).
 *
 * @return 0 on success. Otherwise an error code from error.h.
 */
int tmtc_radio_set_tx_idle_state(uint8_t idle_state)
{
	int err;

	if (idle_state == 0)
		err = IsisTrxvu_tcSetIdlestate(_chosen_radio_index, trxvu_idle_state_off);
	else
		err = IsisTrxvu_tcSetIdlestate(_chosen_radio_index, trxvu_idle_state_on);
	if (err != E_NO_SS_ERR)
		return TMTC_ERR_RADIO_COULD_NOT_SET_IDLE_STATE;

	return 0;
}

/**
 * @brief Sets the radio tranmitter bitrate.
 *
 * @param[in] bitrate Bitrate to set (1200, 2400, 4800 or 9600).
 *
 * @return 0 on success. Otherwise an error code from error.h.
 */
int tmtc_radio_set_tx_bitrate(uint16_t bitrate)
{
	int err;
	ISIStrxvuBitrate trxvu_bitrate;

	switch (bitrate) {
	case 1200:
		trxvu_bitrate = trxvu_bitrate_1200;
		break;
	case 2400:
		trxvu_bitrate = trxvu_bitrate_2400;
		break;
	case 4800:
		trxvu_bitrate = trxvu_bitrate_4800;
		break;
	case 9600:
		trxvu_bitrate = trxvu_bitrate_9600;
		break;
	default:
		return TMTC_ERR_RADIO_TX_INVALID_BITRATE;
	}

	err = IsisTrxvu_tcSetAx25Bitrate(_chosen_radio_index, trxvu_bitrate);
	if (err != E_NO_SS_ERR)
		return TMTC_ERR_RADIO_COULD_NOT_SET_BITRATE;

	return 0;
}

/**
 * @brief Provide data to radio transmitter.
 *
 * @param[in] src Data to transmit.
 * @param[in] len Length of data pointed to by src.
 *
 * @return 0 on success. Otherwise an error code from error.h.
 */
int tmtc_radio_tx(uint8_t *src, size_t len)
{
	int err;
	unsigned char txlen;
	unsigned char avail;

	if (src == NULL)
		return TMTC_ERR_NULL_ARGUMENT;

	if (len > TMTC_TRXVU_DOWNLINK_PAYLOAD_MAXSIZE)
		return TMTC_ERR_RADIO_TX_DATA_TOO_LARGE;

	if (len == 0)
		return TMTC_ERR_RADIO_TX_DATA_TOO_SMALL;

	txlen = (unsigned char) (len & 0xff);

	err = IsisTrxvu_tcSendAX25DefClSign(_chosen_radio_index,
	                                    (unsigned char *) src,
	                                    txlen,
	                                    &avail);
	if (err != E_NO_SS_ERR)
		return TMTC_ERR_RADIO_TX_FAILED;

	if (avail == 255)
		return TMTC_ERR_RADIO_TX_BUFFER_IS_FULL;

	_tmtcvar_last_avail = (uint8_t) avail;

	return 0;
}

/**
 * @brief Extract a frame from the receive buffer.
 *
 * @param[out] dst Where the frame shall be stored. This region must at have a
 *                 capacity of at least #TMTC_TRXVU_UPLINK_PAYLOAD_MAXSIZE
 *                 bytes.
 * @param[out] len Where the length of the received frame shall be written.
 *
 * @return 0 on success. Otherwise an error code from error.h.
 */
int tmtc_radio_rx(uint8_t *dst, size_t *len)
{
	int err;
	ISIStrxvuRxFrame frame;

	if (dst == NULL || len == NULL)
		return TMTC_ERR_NULL_ARGUMENT;

	frame.rx_framedata = (unsigned char *) dst;

	err = IsisTrxvu_rcGetCommandFrame(_chosen_radio_index, &frame);
	if (err != E_NO_SS_ERR)
		return TMTC_ERR_RADIO_RX_FAILED;

	*len = frame.rx_length;

	return 0;
}

/**
 * @brief Get number of frames currently in the receive buffer.
 *
 * @param[out] count Where to write the resulting count.
 *
 * @return 0 on success. Otherwise an error code from error.h.
 */
int tmtc_radio_rxbuf_frame_count(size_t *count)
{
	int err;
	unsigned short framecount;

	if (count == NULL)
		return TMTC_ERR_NULL_ARGUMENT;

	err = IsisTrxvu_rcGetFrameCount(_chosen_radio_index, &framecount);
	if (err != E_NO_SS_ERR)
		return TMTC_ERR_RADIO_RX_COULD_NOT_GET_COUNT;

	*count = framecount;

	return 0;
}

/**
 * @brief Get number of free slots in transmit buffer at the last transmission.
 *
 * @warning The number of available slots in the transmit buffer does not get
 *          reset if either of the Hard Reset or Soft Reset functions are
 *          invoked. It is up to the caller to be aware of this and use this
 *          function responsibly, likely only after successful transmission.
 *
 * @return The number of available slots in the transmit buffer as was recorded
 *         on the last successful transmission.
 */
uint8_t tmtc_radio_txbuf_last_avail_slots(void)
{
	return _tmtcvar_last_avail;
}

static int tmtc_radio_to_7_char_callsign(uint8_t *callsign, uint8_t ssid, unsigned char *dst)
{
	for (uint8_t i = 0; i < 6; i++) {
		if (callsign[i] > 127 || callsign[i] == 0)
			return TMTC_ERR_RADIO_INVALID_CALLSIGN;
		dst[i] = callsign[i];
	}

	if (ssid > 15)
		return TMTC_ERR_RADIO_INVALID_SSID;
	dst[6] = ssid;
	dst[7] = 0;

	return 0;
}
