/**
 * @file ax25.c
 * @author Muhamad Andung Muntaha <muam@kth.se>
 * @author John Wikman
 * @author William Stackenäs
 *
 * @brief AX.25 packet generation matters definition
 *
 * Modified from previous student implementation
 */

#include <stdint.h>
#include <stdlib.h>

#include <tmtc-api/ax25.h>
#include <tmtc-api/error.h>

/**
 * @brief Encodes an AX.25 TC info frame header.
 *
 * This only encodes the header, meaning that the data field is not copied
 * over.
 *
 * @param[out] dst Region to store the the encoded frame header. This region
 *                 must be at least #AX25_TC_HEADER_SIZE bytes in size.
 * @param[out] encsize Where to write the size of the encoded frame header.
 * @param[in] frame The frame header to encode.
 *
 * @return 0 on success. Otherwise an error code from error.h.
 */
int ax25_encode_tc_info(uint8_t *dst, size_t *encsize, const struct ax25_tc_info *frame)
{
	if (dst == NULL || encsize == NULL || frame == NULL)
		return TMTC_ERR_NULL_ARGUMENT;

	dst[0] = (frame->sequence_flag << 6) & 0xC0;

	*encsize = 1;

	return 0;
}

/**
 * @brief Decodes the bytes pointed to by src into an AX.25 TC header.
 *
 * @param[out] frame Where the decoded frame header fields shall be stored.
 * @param[in] src Pointer to the info field of an encoded AX.25 telecommand.
 *
 * @return 0 on success. Otherwise an error code from error.h.
 */
int ax25_decode_tc_info(struct ax25_tc_info *frame, const uint8_t *src)
{
	if (frame == NULL || src == NULL)
		return TMTC_ERR_NULL_ARGUMENT;

	if ((src[0] & 0x3f) != 0)
		return AX25_ERR_TC_INVALID_SPARE;

	frame->sequence_flag = (src[0] >> 6) & 0x3;

	return 0;
}

/**
 * @brief Encodes an AX.25 TM info frame header.
 *
 * This only encodes the header, meaning that the data field or any of the
 * trailer fields are not copied over.
 *
 * @param[out] dst Region to store the the encoded frame header. This region
 *                 must be at least #AX25_TM_HEADER_SIZE bytes in size.
 * @param[out] encsize Where to write the size of the encoded frame header.
 * @param[in] frame The frame whose header to encode.
 *
 * @return 0 on success. Otherwise an error code from error.h.
 */
int ax25_encode_tm_info_header(uint8_t *dst, size_t *encsize, const struct ax25_tm_info *frame)
{
	if (dst == NULL || encsize == NULL || frame == NULL)
		return TMTC_ERR_NULL_ARGUMENT;

	/* Encode Frame identification */
	dst[0] = (frame->virtual_channel_id << 3) & 0x38;

	/* Encode remaining header fields */
	dst[1] = frame->master_frame_count;
	dst[2] = frame->virtual_channel_frame_count;
	dst[3] = frame->first_header_pointer;

	*encsize = 4;

	return 0;
}

/**
 * @brief Decodes bytes into an AX.25 TM header.
 *
 * This only decodes the Telemetry Transfer Frame Secondary Header. The
 * trailer fields are decoded separately.
 *
 * @param[out] frame Where the decoded frame header fields shall be stored.
 * @param[in] src Pointer to the start of the info field of an encoded AX.25
 *                telemetry.
 *
 * @return 0 on success. Otherwise an error code from error.h.
 */
int ax25_decode_tm_info_header(struct ax25_tm_info *frame, const uint8_t *src)
{
	if (frame == NULL || src == NULL)
		return TMTC_ERR_NULL_ARGUMENT;

	/* Decode Frame identification */
	if ((src[0] & 0xC0) != 0)
		return AX25_ERR_TM_INVALID_VERSION_NUMBER;
	if ((src[0] & 0x07) != 0)
		return AX25_ERR_TM_INVALID_SPARE;

	frame->virtual_channel_id = (src[0] >> 3) & 0x7;

	/* Decode remaining header fields */
	frame->master_frame_count = src[1];
	frame->virtual_channel_frame_count = src[2];
	frame->first_header_pointer = src[3];

	return 0;
}

/**
 * @brief Encodes an AX.25 TM info frame trailer.
 *
 * This only encodes the trailer, meaning that the none of the header fields or
 * data fields are copied over.
 *
 * @param[out] dst Region to store the the encoded frame trailer. This region
 *                 must be at least #AX25_TM_TRAILER_MAXSIZE bytes in size.
 * @param[out] encsize Where to write the size of the encoded frame trailer.
 * @param[in] frame The frame whose trailer to encode.
 *
 * @return 0 on success. Otherwise an error code from error.h.
 */
int ax25_encode_tm_info_trailer(uint8_t *dst, size_t *encsize, const struct ax25_tm_info *frame)
{
	if (dst == NULL || encsize == NULL || frame == NULL)
		return TMTC_ERR_NULL_ARGUMENT;

	dst[0] = frame->tc_count & 0x3;

	if (frame->time_flag) {
		dst[0] |= 0xb0; /* Timeflag: 0b1011---- */
		dst[1] = (frame->time >> 24) & 0xff;
		dst[2] = (frame->time >> 16) & 0xff;
		dst[3] = (frame->time >> 8) & 0xff;
		dst[4] = frame->time & 0xff;
		*encsize = 5;
	} else {
		*encsize = 1;
	}

	return 0;
}

/**
 * @brief Decodes bytes into an AX.25 TM info frame trailer.
 *
 * This only decodes the trailer. The Telemetry Transfer Frame Secondary Header
 * and the data fields are decoded separately.
 *
 * @param[out] frame Where the decoded frame trailer fields shall be stored.
 * @param[in] src Pointer to the trailer in the info field of an encoded AX.25
 *                telemetry.
 *
 * @return 0 on success. Otherwise an error code from error.h.
 */
int ax25_decode_tm_info_trailer(struct ax25_tm_info *frame, const uint8_t *src)
{
	if (frame == NULL || src == NULL)
		return TMTC_ERR_NULL_ARGUMENT;

	/* Decode Frame Status */
	if ((src[0] & 0x0C) != 0)
		return AX25_ERR_TM_INVALID_SPARE;

	frame->tc_count = src[0] & 0x3;

	/* Decode Time & Time Flag */
	if ((src[0] & 0xf0) == 0xb0) {
		frame->time_flag = 1;
		frame->time = (src[1] << 24) |
		              (src[2] << 16) |
		              (src[3] << 8) |
		              src[4];
	} else if ((src[0] & 0xf0) == 0) {
		frame->time_flag = 0;
	} else {
		return AX25_ERR_TM_INVALID_TIME_FLAG;
	}

	return 0;
}
