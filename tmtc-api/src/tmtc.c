/**
 * @file tmtc.c
 * @author Muhamad Andung Muntaha <muam@kth.se>
 * @author John Wikman
 * @author Sonal Shrivastava
 * @author William Stackenäs
 *
 * @brief tmtc-api function implementation
 *
 * This consists of all functions an OBC programmer needs which provide a
 * simplification in communicating with the ground station.
 *
 */

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <tmtc-api/crc.h>
#include <tmtc-api/error.h>
#include <tmtc-api/radio.h>
#include <tmtc-api/service_types.h>
#include <tmtc-api/tmtc.h>
#include <tmtc-api/tmtc_callbacks.h>

static int tmtcvar_is_initialized = 0;

/**
 * Initializes and sets up the TMTC API.
 *
 * @param[in] sim An optional radio simulator. If not NULL, The simulator will
 *                be probed using an I2C command and used instead of the real
 *                radio if it replies
 *
 * @return 0 on success. Otherwise an error code from error.h.
 */
int tmtc_setup(struct tmtc_radio *sim)
{
	int err;

	tmtc_crc16_initialize_table();

	err = tmtc_radio_initialize(sim);
	if (err == 0 || err == TMTC_WARN_UNREACHABLE) {
		tmtcvar_is_initialized = 1;
	}

	return err;
}

/**
 * @brief Retrieves and decodes a telecommand from the radio.
 *
 * @param[out] tc Where the retrieved telecommand shall be stored.
 *
 * @return 0 on success. Otherwise an error code from error.h.
 */
int tmtc_get_tc(struct tmtc_telecommand *tc)
{
	int err;
	size_t frame_count;

	if (!tmtcvar_is_initialized)
		return TMTC_ERR_NOT_INITIALIZED;

	if (tc == NULL)
		return TMTC_ERR_NULL_ARGUMENT;

	/* Make sure that there is a frame to receive */
	err = tmtc_radio_rxbuf_frame_count(&frame_count);
	if (err)
		return err;
	if (frame_count == 0)
		return TMTC_ERR_RADIO_RX_BUFFER_IS_EMPTY;

	err = tmtc_radio_rx(tc->rawdata, &tc->rawlength);
	if (err)
		return err;

	/* Decode AX.25 and CCSDS before returning */
	err = ax25_decode_tc_info(&tc->ax25, &tc->rawdata[0]);
	if (err)
		return err;

	err = ccsds_decode_tc(&tc->ccsds, &tc->rawdata[AX25_TC_HEADER_SIZE]);
	if (err)
		return err;

	return 0;
}

/**
 * @brief Encodes and sends a telemetry frame.
 *
 * @param[out] tm The telemetry frame to be sent.
 *
 * @return 0 on success. Otherwise an error code from error.h.
 */
int tmtc_send_tm(struct tmtc_telemetry *tm)
{
	int err;
	size_t index;
	size_t enclen;

	if (!tmtcvar_is_initialized)
		return TMTC_ERR_NOT_INITIALIZED;

	if (tm == NULL)
		return TMTC_ERR_NULL_ARGUMENT;

	index = 0;

	/* Encode AX.25 header, CCSDS packet, then AX.25 trailer */
	err = ax25_encode_tm_info_header(&tm->rawdata[index], &enclen, &tm->ax25);
	if (err)
		return err;

	index += enclen;

	err = ccsds_encode_tm(&tm->rawdata[index], &enclen, &tm->ccsds, NULL);
	if (err)
		return err;

	index += enclen;

	err = ax25_encode_tm_info_trailer(&tm->rawdata[index], &enclen, &tm->ax25);
	if (err)
		return err;

	index += enclen;

	/* index now contains the length of the encoded packet */
	tm->rawlength = index;

	err = tmtc_radio_tx(tm->rawdata, tm->rawlength);
	if (err)
		return err;

	return 0;
}

/**
 * @brief Generates a standard telemetry based on the provided parameters.
 *
 * This only fills out the source data and the AX.25 and CCSDS fields of the
 * telemetry. It does not encode telemetry. After successfully invoking this
 * function, the generated telemetry is ready to be sent in tmtc_send_tm().
 *
 * @param[out] tm Where to store the generated telemetry fields.
 * @param[in] params A struct containing the parameters of which the telemetry
 *                   should be generated from. All of the fields in this struct
 *                   must be filled out.
 *
 * @return 0 on success. Otherwise an error code from error.h.
 */
int tmtc_generate_tm(struct tmtc_telemetry *tm, const struct tmtc_tm_params *params)
{
	if (!tmtcvar_is_initialized)
		return TMTC_ERR_NOT_INITIALIZED;

	if (tm == NULL || params == NULL)
		return TMTC_ERR_NULL_ARGUMENT;

	tm->ax25.virtual_channel_id = params->virtual_channel_id;
	tm->ax25.master_frame_count = tmtc_ax25_master_frame_count_cb();
	tm->ax25.virtual_channel_frame_count = tmtc_ax25_virtual_frame_count_cb(params->virtual_channel_id);
	tm->ax25.first_header_pointer = 0x00; //< based on old implementation
	tm->ax25.time_flag = 0;
	tm->ax25.tc_count = 0x3; //< based on old implementation

	tm->ccsds.apid = params->apid;
	tm->ccsds.source_sequence_count = tmtc_ccsds_sequence_count_cb(params->apid);
	tm->ccsds.packet_length = CCSDS_TM_PACKET_LENGTH(params->payloadlen);
	tm->ccsds.service_type = params->service_type;
	tm->ccsds.service_subtype = params->service_subtype;
	tm->ccsds.time = params->time;
	memcpy(TM_CCSDS_SOURCE_DATA(tm->rawdata), params->payload, params->payloadlen);

	return 0;
}

/**
 * @brief Generates an acknowledgment for a telecommand.
 *
 * This only fills out the source data and the AX.25 and CCSDS fields of the
 * telemetry. It does not encode telemetry. After successfully invoking this
 * function, the generated telemetry is ready to be sent in tmtc_send_tm().
 *
 * @param[out] ack Where to store the generated acknowledgment fields.
 * @param[in] params A struct containing the parameters from which the
 *                   acknowledgment should be generated from. All of the fields
 *                   in this struct must be filled out and the ackinfo_buf field
 *                   shall not be modified before the generated acknowledgment
 *                   has been encoded.
 *
 * @return 0 on success. Otherwise an error code from error.h.
 */
int tmtc_generate_ack(struct tmtc_telemetry *ack, struct tmtc_ack_params *params)
{
	if (!tmtcvar_is_initialized)
		return TMTC_ERR_NOT_INITIALIZED;

	if (ack == NULL || params == NULL)
		return TMTC_ERR_NULL_ARGUMENT;

	ack->ax25.virtual_channel_id = params->virtual_channel_id;
	ack->ax25.master_frame_count = tmtc_ax25_master_frame_count_cb();
	ack->ax25.virtual_channel_frame_count = tmtc_ax25_virtual_frame_count_cb(params->virtual_channel_id);
	ack->ax25.first_header_pointer = 0x00; //< based on old implementation
	ack->ax25.time_flag = 0;
	ack->ax25.tc_count = 0x3; //< based on old implementation

	ack->ccsds.apid = params->tc_apid;
	ack->ccsds.source_sequence_count = params->tc_sequence_count;
	ack->ccsds.service_type = TMTC_ST_ACK;
	ack->ccsds.service_subtype = params->ack_subtype;
	ack->ccsds.time = params->time;

	/*
	 * Note:
	 * Original TMTC API suggests that the first short should consist of the
	 * entire packet-id field from the telecommand, and that the second short
	 * should consist of the entire packet sequence control. However, due to a
	 * bug in the original TMTC API, this was never case and the only thing
	 * that happened was that the LSBit could on occasion have its value
	 * flipped. Assuming that it worked fine like that, then we go with this
	 * here as well.
	 */
	params->ackinfo_buf[0] = (params->tc_apid >> 8) & 0x7;
	params->ackinfo_buf[1] = params->tc_apid & 0xFF;
	params->ackinfo_buf[2] = (params->tc_sequence_count >> 8) & 0x3F;
	params->ackinfo_buf[3] = params->tc_sequence_count & 0xFF;
	params->ackinfo_buf[4] = (params->ack_error_code >> 8) & 0xFF;
	params->ackinfo_buf[5] = params->ack_error_code & 0xFF;

	memcpy(TM_CCSDS_SOURCE_DATA(ack->rawdata), params->ackinfo_buf, 6);
	ack->ccsds.packet_length = CCSDS_TM_PACKET_LENGTH(6);

	return 0;
}

/**
 * @brief Decodes the raw data of a telecommand, ignoring CCSDS CRC check.
 *
 * The decoded packet info will be entered into the tc->ax25 and tc->ccsds
 * fields.
 *
 * @warning This function should only ever be used in the case that this packet
 *          has been decoded before.
 *
 * @param[in,out] tc Telecommand whose raw data is to be decoded.
 *
 * @return 0 on success. Otherwise an error code from error.h.
 */
int tmtc_decode_tc_ignorecrc(struct tmtc_telecommand *tc)
{
	int err;

	if (!tmtcvar_is_initialized)
		return TMTC_ERR_NOT_INITIALIZED;

	if (tc == NULL)
		return TMTC_ERR_NULL_ARGUMENT;

	/* Decode AX.25 */
	err = ax25_decode_tc_info(&tc->ax25, &tc->rawdata[0]);
	if (err)
		return err;

	err = ccsds_decode_tc_ignorecrc(&tc->ccsds, &tc->rawdata[AX25_TC_HEADER_SIZE]);
	if (err)
		return err;

	return 0;
}

/**
 * @brief Generates an encoded telecommand from given telcommand parameters.
 *
 * @param[out] tc Pointer to variable of type #tmtc_telecommand to encode telecommand information in.
 * @param[in] tc_params Pointer to variable of type #tmtc_tc_params to extract TC information from.
 *
 * @return 0 on success. Otherwise an error code from error.h.
 */
int tmtc_encode_tc(struct tmtc_telecommand *tc, const struct tmtc_tc_params *tc_params)
{
	int err;
	size_t ax25_encsize = 0;
	size_t ccsds_encsize = 0;

	if (tc == NULL || tc_params == NULL)
		return TMTC_ERR_NULL_ARGUMENT;

	tc->ax25.sequence_flag = tc_params->sequence_flag;
	tc->ccsds.apid = tc_params->apid;
	tc->ccsds.sequence_count = tc_params->tc_seq_count;
	tc->ccsds.packet_length = CCSDS_TC_PACKET_LENGTH(tc_params->app_data_length);
	tc->ccsds.ack = tc_params->ack_acceptance << 3 |
	                tc_params->ack_exec_start << 2 |
	                tc_params->ack_exec_progress << 1 |
	                tc_params->ack_exec_completion;
	tc->ccsds.service_type = tc_params->service_type;
	tc->ccsds.service_subtype = tc_params->service_subtype;

	err = ax25_encode_tc_info(tc->rawdata, &ax25_encsize, &tc->ax25);
	if (err)
		return err;

	err = ccsds_encode_tc(&tc->rawdata[AX25_TC_HEADER_SIZE],
	                      &ccsds_encsize,
	                      &tc->ccsds,
	                      tc_params->application_data);
	if (err)
		return err;

	tc->rawlength = ax25_encsize + ccsds_encsize;

	return 0;
}
