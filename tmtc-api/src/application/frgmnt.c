/**
 * @file    frgmnt.c
 * @author  Erik Flink
 *
 * Fragmentation of data blocks.
 */

#include <string.h>
#include <inttypes.h>
#include <stddef.h>

#include <tmtc-api/error.h>
#include <tmtc-api/application/frgmnt.h>

/**
 * Initializes a fragmenter.
 * The block ID is incremented with each call
 *
 * @param f             Pointer to a fragmenter, for storage of the fragmentation state.
 * @param block         Pointer to the block of data that will be fragmented.
 * @param block_size    The size of the data to be fragmented.
 * @param fragment_size The maximum size of the fragments.
 * @param fragment_count Where to store the number of fragments that the block will be divided into
 *
 * @return 0 on success. Otherwise an error code from error.h
 */
int frgmnt_init_fragmentation(fragmenter_t *f, const uint8_t *block, uint16_t block_size, uint8_t fragment_size, int *fragment_count)
{
	if (f == NULL || block == NULL || fragment_count == NULL)
	{
		return TMTC_ERR_NULL_ARGUMENT;
	}

	if (fragment_size < 3 || block_size > FRGMNT_BLOCK_MAXSIZE(fragment_size))
	{
		return TMTC_ERR_SIZE;
	}

	f->block = block;
	f->block_size = block_size;
	f->block_id++;
	f->fragment_size = fragment_size;
	f->fragment_number = 0;
	f->offset = 0;

	if (block_size <= fragment_size - 3)
	{
		f->tail_count = 0;
	}
	else
	{
		f->tail_count = (block_size - (fragment_size - 3)) / (fragment_size - 3) +
		                ((block_size - (fragment_size - 3)) % (fragment_size - 3) != 0);
	}

	*fragment_count = f->tail_count + 1;

	return 0;
}

/**
 * Gets the next fragment and copies it into the provided buffer.
 * The fragment number is incremented with each call.
 *
 * @param f           A fragmenter that has been initialized.
 * @param fragmentbuf Pointer to a buffer big enough to store the fragment.
 * @param fragment_size Where to store the size of the fragment that was copied into the buffer
 *
 * @return 0 on success. Otherwise an error code from error.h
 */
int frgmnt_get_next_fragment(fragmenter_t *f, uint8_t *fragmentbuf, uint8_t *fragment_size)
{
	uint8_t i;

	if (f == NULL || fragmentbuf == NULL || fragment_size == NULL)
	{
		return TMTC_ERR_NULL_ARGUMENT;
	}

	if (f->block == NULL ||
	    f->fragment_number > f->tail_count ||
	    f->offset > f->block_size ||
	    f->fragment_size < 3 ||
	    f->block_size > FRGMNT_BLOCK_MAXSIZE(f->fragment_size))
	{
		return TMTC_ERR_NOT_INITIALIZED;
	}

	fragmentbuf[0] = f->block_id;
	fragmentbuf[1] = f->fragment_number;
	fragmentbuf[2] = f->tail_count;
	i = 3;

	while (i < f->fragment_size && f->offset < f->block_size)
	{
		fragmentbuf[i++] = f->block[f->offset++];
	}

	if (f->fragment_number == f->tail_count)
	{
		f->block = NULL;
	}
	else
	{
		f->fragment_number++;
	}

	*fragment_size = i;

	return 0;
}
