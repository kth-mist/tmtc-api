/*!
 * @file crc.c
 *
 * @brief CRC calculation implementation for checking checksum.
 *
 * Based of A.1.6 from EECS-E-70-41A (2003-01-30)
 *
 * TMTC_CRC16 Compliance Verification:
 * 00 00 -> 1D 0F
 * 00 00 00 -> CC 9C
 * AB CD EF 01 -> 04 A2
 * 14 56 F8 9A 00 01 -> 7F D5
 */

#include <stdint.h>

#include <tmtc-api/crc.h>
#include <tmtc-api/error.h>

static uint16_t tmtc_crc16_table[256];
static int tmtc_crc16_initialized = 0;

/**
 * @brief Initializes the CRC table.
 */
void tmtc_crc16_initialize_table(void)
{
    int i;
    uint16_t tmp;

    for (i = 0; i < 256; i++) {
        tmp = 0;

        if ((i & 0x01) != 0)
            tmp = tmp ^ 0x1021;
        if ((i & 0x02) != 0)
            tmp = tmp ^ 0x2042;
        if ((i & 0x04) != 0)
            tmp = tmp ^ 0x4084;
        if ((i & 0x08) != 0)
            tmp = tmp ^ 0x8108;
        if ((i & 0x10) != 0)
            tmp = tmp ^ 0x1231;
        if ((i & 0x20) != 0)
            tmp = tmp ^ 0x2462;
        if ((i & 0x40) != 0)
            tmp = tmp ^ 0x48C4;
        if ((i & 0x80) != 0)
            tmp = tmp ^ 0x9188;

        tmtc_crc16_table[i] = tmp;
    }

    tmtc_crc16_initialized = 1;
}

/**
 * @brief Computes CRC-16 checksum
 *
 * @param[in] data Data to have its checksum calculated.
 * @param[in] length The length of the data to have its checksum calculated.
 * @param[out] chksum Where to store the calculated checksum.
 *
 * @return 0 on success. Otherwise an error code from error.h.
 */
int tmtc_crc16(const uint8_t *data, size_t length, uint16_t *chksum)
{
    size_t i;
    uint16_t tmpchk;

    if (data == NULL || chksum == NULL)
        return TMTC_ERR_CRC_NULL_ARGUMENT;

    if (!tmtc_crc16_initialized)
        tmtc_crc16_initialize_table();

    tmpchk = 0xffff;

    for (i = 0; i < length; i++)
        tmpchk = ((tmpchk << 8) & 0xff00) ^ tmtc_crc16_table[((tmpchk >> 8) ^ data[i]) & 0xff];

    *chksum = tmpchk;
    return 0;
}

/**
 * @brief Calculate CRC value for a single byte.
 *
 * Warning: This functions assumes that tmtc_crc16_initialize_table() has been
 * invoked prior to this function.
 *
 * @param [in] data The byte to compute checksum for.
 * @param [in] check The intermediate check value.
 *
 * @return Two bytes CRC value.
 */
uint16_t tmtc_crc16_optimized(uint8_t data, uint16_t check)
{
    return (((check << 8) & 0xFF00) ^ tmtc_crc16_table[((check >> 8) ^ data) & 0xFF]);
}
