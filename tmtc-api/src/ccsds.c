/**
 * @file ccsds.c
 * @author Muhamad Andung Muntaha <muam@kth.se>
 * @author John Wikman
 * @author William Stackenäs
 *
 * @brief CCSDS object and function definitions
 *
 * Modified from previous student implementation
 */

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <tmtc-api/ccsds.h>
#include <tmtc-api/crc.h>
#include <tmtc-api/error.h>

/**
 * @brief Encodes a CCSDS telecommand packet as a sequence of bytes.
 *
 * @param[out] dst Region to store the the encoded packet. This region must be
 *                 at least #CCSDS_TC_ENCODED_MAXLENGTH bytes in size.
 * @param[out] encsize Where to write the size of the encoded packet.
 * @param[in] pkt The packet to encode.
 * @param[in] application_data A pointer to the application data of the packet to encode.
 *                             If NULL, it is assumed that the source data has already
 *                             been encoded.
 *
 * @return 0 on success. Otherwise an error code from error.h.
 */
int ccsds_encode_tc(uint8_t *dst, size_t *encsize, const struct ccsds_tc *pkt, const uint8_t *application_data)
{
	int err;
	size_t application_length;
	uint16_t packet_error_control;

	if (dst == NULL || encsize == NULL || pkt == NULL)
		return TMTC_ERR_NULL_ARGUMENT;

	if ((pkt->packet_length + 1) > (CCSDS_TC_ENCODED_MAXLENGTH - CCSDS_PACKET_HEADER_SIZE))
		return CCSDS_ERR_PACKET_LENGTH_TOO_LARGE;
	if ((pkt->packet_length + 1) < CCSDS_TC_PACKET_DATA_FIELD_METADATA_SIZE)
		return CCSDS_ERR_PACKET_LENGTH_TOO_SMALL;

	/* According to the specification, all fields shall be encoded in
	 * big-endian byte and bit format. */

	/* Encode Packet ID */
	dst[0] = 0x18; /* 0b00011000: type & data field header flag */
	dst[0] |= (pkt->apid >> 8) & 0x7;
	dst[1] = pkt->apid & 0xFF;

	/* Encode Packet Sequence Control */
	dst[2] = 0xC0; /* 0b11000000: sequence flags */
	dst[2] |= (pkt->sequence_count >> 8) & 0x3F;
	dst[3] = pkt->sequence_count & 0xFF;

	/* Encode Packet Length */
	dst[4] = (pkt->packet_length >> 8) & 0xFF;
	dst[5] = pkt->packet_length & 0xFF;

	/* Encode Telecommand Data Field Header */
	dst[6] = 0x10; /* 0b00010000: secondary header flag & version number */
	dst[6] |= pkt->ack & 0xF;
	dst[7] = pkt->service_type;
	dst[8] = pkt->service_subtype;

	/* Copy over application data */
	application_length = CCSDS_TC_APPLICATION_DATA_LENGTH(pkt->packet_length);
	if (application_data != NULL) {
		memcpy(&dst[9], application_data, application_length);
	}

	/* Compute Packet Error Control */
	err = tmtc_crc16(dst, 9 + application_length, &packet_error_control);
	if (err)
		return err;

	dst[9 + application_length + 0] = (packet_error_control >> 8) & 0xFF;
	dst[9 + application_length + 1] = packet_error_control & 0xFF;

	*encsize = 9 + application_length + 2;
	return 0;
}

/**
 * @brief Decodes the sequence of bytes pointed to by src into a ccsds tc.
 *
 * @param[out] pkt Where the decoded packet fields shall be stored.
 * @param[in] src Pointer to an encoded ccsds telecommand.
 *
 * @return 0 on success. Otherwise an error code from error.h.
 */
int ccsds_decode_tc(struct ccsds_tc *pkt, const uint8_t *src)
{
	int err;
	size_t application_length;
	uint16_t decoded_packet_error_control;
	uint16_t generated_packet_error_control;

	err = ccsds_decode_tc_ignorecrc(pkt, src);
	if (err)
		return err;

	/* Decode Application Data */
	application_length = CCSDS_TC_APPLICATION_DATA_LENGTH(pkt->packet_length);

	/* Verify Packet Error Control */
	decoded_packet_error_control = (src[9 + application_length + 0] << 8) |
	                               src[9 + application_length + 1];

	err = tmtc_crc16(src, 9 + application_length, &generated_packet_error_control);
	if (err)
		return err;

	if (decoded_packet_error_control != generated_packet_error_control)
		return CCSDS_ERR_CRC_MISMATCH;

	return 0;
}

/**
 * @brief Decodes the sequence of bytes pointed to by src into a ccsds tc,
 *        but without the CRC check.
 *
 * @param[out] pkt Where the decoded packet fields shall be stored.
 * @param[in] src Pointer to an encoded ccsds telecommand.
 *
 * @return 0 on success. Otherwise an error code from error.h.
 */
int ccsds_decode_tc_ignorecrc(struct ccsds_tc *pkt, const uint8_t *src)
{
	if (pkt == NULL || src == NULL)
		return TMTC_ERR_NULL_ARGUMENT;

	/* Decode Packed ID */
	if ((src[0] & 0xE0) != 0)
		return CCSDS_ERR_TC_INVALID_VERSION_NUMBER;
	if ((src[0] & 0x10) != 0x10)
		return CCSDS_ERR_TC_INVALID_TYPE;
	if ((src[0] & 0x08) != 0x08)
		return CCSDS_ERR_TC_INVALID_DATA_FIELD_HEADER_FLAG;

	pkt->apid = ((src[0] & 0x07) << 8) | src[1];

	/* Decode Packet Sequence Control */
	if ((src[2] & 0xC0) != 0xC0)
		return CCSDS_ERR_TC_INVALID_SEQUENCE_FLAGS;

	pkt->sequence_count = ((src[2] & 0x3F) << 8) | src[3];

	/* Decode Packet Length */
	pkt->packet_length = (src[4] << 8) | src[5];

	if ((pkt->packet_length + 1) > (CCSDS_TC_ENCODED_MAXLENGTH - CCSDS_PACKET_HEADER_SIZE))
		return CCSDS_ERR_PACKET_LENGTH_TOO_LARGE;
	if ((pkt->packet_length + 1) < CCSDS_TC_PACKET_DATA_FIELD_METADATA_SIZE)
		return CCSDS_ERR_PACKET_LENGTH_TOO_SMALL;

	/* Decode Telecommand Data Field Header */
	if ((src[6] & 0x80) != 0)
		return CCSDS_ERR_TC_INVALID_SECONDARY_HEADER_FLAG;
	if ((src[6] & 0x70) != 0x10)
		return CCSDS_ERR_TC_INVALID_PACKET_PUS_VERSION_NUMBER;

	pkt->ack = src[6] & 0xF;
	pkt->service_type = src[7];
	pkt->service_subtype = src[8];

	/* Ignore the CRC check that would usually be here. */

	return 0;
}


/**
 * @brief Encodes a CCSDS telemetry packet as a sequence of bytes.
 *
 * @param[out] dst Region to store the the encoded packet. This region must be
 *                 at least #CCSDS_TM_ENCODED_MAXLENGTH bytes in size.
 * @param[out] encsize Where to write the size of the encoded packet.
 * @param[in] pkt The packet to encode.
 * @param[in] source_data A pointer to the source data of the packet to encode.
 *                        If NULL, it is assumed that the source data has already
 *                        been encoded.
 *
 * @return 0 on success. Otherwise an error code from error.h.
 */
int ccsds_encode_tm(uint8_t *dst, size_t *encsize, const struct ccsds_tm *pkt, const uint8_t *source_data)
{
	int err;
	size_t source_length;
	uint16_t packet_error_control;

	if (dst == NULL || encsize == NULL || pkt == NULL)
		return TMTC_ERR_NULL_ARGUMENT;

	if ((pkt->packet_length + 1) > (CCSDS_TM_ENCODED_MAXLENGTH - CCSDS_PACKET_HEADER_SIZE))
		return CCSDS_ERR_PACKET_LENGTH_TOO_LARGE;
	if ((pkt->packet_length + 1) < CCSDS_TM_PACKET_DATA_FIELD_METADATA_SIZE)
		return CCSDS_ERR_PACKET_LENGTH_TOO_SMALL;

	/* According to the specification, all fields shall be encoded in
	 * big-endian byte and bit format. */

	/* Encode Packet ID */
	dst[0] = 0x08; /* 0b00001000: type & data field header flag */
	dst[0] |= (pkt->apid >> 8) & 0x7;
	dst[1] = pkt->apid & 0xFF;

	/* Encode Packet Sequence Control */
	dst[2] = 0xC0; /* 0b11000000: grouping flags */
	dst[2] |= (pkt->source_sequence_count >> 8) & 0x3F;
	dst[3] = pkt->source_sequence_count & 0xFF;

	/* Encode Packet Length */
	dst[4] = (pkt->packet_length >> 8) & 0xFF;
	dst[5] = pkt->packet_length & 0xFF;

	/* Encode Telemetry Data Field Header */
	dst[6] = 0x10; /* 0b00010000: source packet PUS version number */
	dst[7] = pkt->service_type;
	dst[8] = pkt->service_subtype;

	/* Time: PTC=9 PFC=16 */
	dst[9] = (pkt->time >> 24) & 0xFF;
	dst[10] = (pkt->time >> 16) & 0xFF;
	dst[11] = (pkt->time >> 8) & 0xFF;
	dst[12] = (pkt->time) & 0xFF;
	dst[13] = 0;

	/* Copy over source data */
	source_length = CCSDS_TM_SOURCE_DATA_LENGTH(pkt->packet_length);
	if (source_data != NULL) {
		memcpy(&dst[14], source_data, source_length);
	}

	/* Compute Packet Error Control */
	err = tmtc_crc16(dst, 14 + source_length, &packet_error_control);
	if (err)
		return err;

	dst[14 + source_length + 0] = (packet_error_control >> 8) & 0xFF;
	dst[14 + source_length + 1] = packet_error_control & 0xFF;

	*encsize = 14 + source_length + 2;
	return 0;
}

/**
 * @brief Decodes the sequence of bytes pointed to by src into a ccsds tm.
 *
 * @param[out] pkt Where the fields of the decoded packet shall be stored.
 * @param[in] src Pointer to an encoded ccsds telemetry.
 *
 * @return 0 on success. Otherwise an error code from error.h.
 */
int ccsds_decode_tm(struct ccsds_tm *pkt, const uint8_t *src)
{
	int err;
	size_t source_length;
	uint16_t decoded_packet_error_control;
	uint16_t generated_packet_error_control;

	if (pkt == NULL || src == NULL)
		return TMTC_ERR_NULL_ARGUMENT;

	/* Decode Packed ID */
	if ((src[0] & 0xE0) != 0)
		return CCSDS_ERR_TM_INVALID_VERSION_NUMBER;
	if ((src[0] & 0x10) != 0)
		return CCSDS_ERR_TM_INVALID_TYPE;
	if ((src[0] & 0x08) != 0x08)
		return CCSDS_ERR_TM_INVALID_DATA_FIELD_HEADER_FLAG;

	pkt->apid = ((src[0] & 0x07) << 8) | src[1];

	/* Decode Packet Sequence Control */
	if ((src[2] & 0xC0) != 0xC0)
		return CCSDS_ERR_TM_INVALID_GROUPING_FLAGS;

	pkt->source_sequence_count = ((src[2] & 0x3f) << 8) | src[3];

	/* Decode Packet Length */
	pkt->packet_length = (src[4] << 8) | src[5];

	if ((pkt->packet_length + 1) > (CCSDS_TM_ENCODED_MAXLENGTH - CCSDS_PACKET_HEADER_SIZE))
		return CCSDS_ERR_PACKET_LENGTH_TOO_LARGE;
	if ((pkt->packet_length + 1) < CCSDS_TM_PACKET_DATA_FIELD_METADATA_SIZE)
		return CCSDS_ERR_PACKET_LENGTH_TOO_SMALL;

	/* Decode Telemetry Data Field Header */
	if ((src[6] & 0x80) != 0)
		return CCSDS_ERR_TM_INVALID_SPARE; // (invalid 1st spare)
	if ((src[6] & 0x70) != 0x10)
		return CCSDS_ERR_TM_INVALID_SOURCE_PACKET_PUS_VERSION_NUMBER;
	if ((src[6] & 0x0F) != 0)
		return CCSDS_ERR_TM_INVALID_SPARE; // (invalid 2nd spare)

	pkt->service_type = src[7];
	pkt->service_subtype = src[8];

	/* Time: PTC=9 PFC=16 */
	pkt->time = (src[9] << 24) |
	            (src[10] << 16) |
	            (src[11] << 8) |
	            src[12];

	/* Decode Source Data */
	source_length = CCSDS_TM_SOURCE_DATA_LENGTH(pkt->packet_length);

	/* Verify Packet Error Control */
	decoded_packet_error_control = (src[14 + source_length + 0] << 8) |
	                               src[14 + source_length + 1];

	err = tmtc_crc16(src, 14 + source_length, &generated_packet_error_control);
	if (err)
		return err;

	if (decoded_packet_error_control != generated_packet_error_control)
		return CCSDS_ERR_CRC_MISMATCH;

	return 0;
}
