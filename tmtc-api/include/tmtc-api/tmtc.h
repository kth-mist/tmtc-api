/**
 * @file tmtc.h
 * @author Muhamad Andung Muntaha <muam@kth.se>
 * @author John Wikman
 * @author William Stackenäs
 *
 * @brief tmtc-api function definition
 *
 */

#ifndef TMTCAPI_TMTC_H
#define TMTCAPI_TMTC_H

#include "ax25.h"
#include "ccsds.h"
#include "radio.h"
#include "config/config.h"

#define TC_AX25_DATA(raw_tc) (&(raw_tc)[AX25_TC_DATA_OFFSET])
#define TM_AX25_DATA(raw_tm) (&(raw_tm)[AX25_TM_DATA_OFFSET])

#define TC_CCSDS_APPLICATION_DATA(raw_tc)  (&(raw_tc)[AX25_TC_DATA_OFFSET + CCSDS_TC_APPLICATION_DATA_OFFSET])
#define TM_CCSDS_SOURCE_DATA(raw_tm)       (&(raw_tm)[AX25_TM_DATA_OFFSET + CCSDS_TM_SOURCE_DATA_OFFSET])

/**
 * @brief A telecommand struct containing raw data and parsed fields.
 */
struct tmtc_telecommand {
	struct ax25_tc_info ax25;
	struct ccsds_tc ccsds;

	uint8_t rawdata[TMTC_TRXVU_UPLINK_PAYLOAD_MAXSIZE];
	size_t rawlength;
};

/**
 * @brief A telemetry struct containing raw data and parsed fields.
 */
struct tmtc_telemetry {
	struct ax25_tm_info ax25;
	struct ccsds_tm ccsds;

	uint8_t rawdata[TMTC_TRXVU_DOWNLINK_PAYLOAD_MAXSIZE];
	size_t rawlength;
};

/**
 * @brief A struct containing parameters for generating a telemetry packet.
 */
struct tmtc_tm_params {
	/* AX.25 params */
	uint8_t virtual_channel_id : 3;

	/* CCSDS params */
	uint16_t apid : 11;
	uint8_t service_type;
	uint8_t service_subtype;
	uint32_t time;

	const uint8_t *payload;
	size_t payloadlen;
};

/**
 * @brief A struct containing parameters for generating an acknowledgment.
 */
struct tmtc_ack_params {
	/* AX.25 params */
	uint8_t virtual_channel_id : 3;

	/* CCSDS params */
	uint8_t ack_subtype;
	uint32_t time;

	/* Acknowledgment Information */
	uint16_t tc_apid : 11;
	uint16_t tc_sequence_count : 14;
	uint16_t ack_error_code;

	/* This field serves as a buffer and does not need to be filled out */
	uint8_t ackinfo_buf[6];
};

/**
 * @brief A struct containing parameters for generating a telecommand packet.
 */
struct tmtc_tc_params {
	/* AX.25 params */
	uint8_t sequence_flag : 2;

	/* CCSDS params */
	uint16_t apid : 11;
	uint16_t tc_seq_count : 14;
	uint8_t app_data_length;
	uint8_t ack_acceptance : 1;
	uint8_t ack_exec_start : 1;
	uint8_t ack_exec_progress : 1;
	uint8_t ack_exec_completion : 1;
	uint8_t service_type;
	uint8_t service_subtype;
	const uint8_t *application_data;
};

int tmtc_setup(struct tmtc_radio *sim);

int tmtc_get_tc(struct tmtc_telecommand *tc);
int tmtc_send_tm(struct tmtc_telemetry *tm);

int tmtc_generate_tm(struct tmtc_telemetry *tm, const struct tmtc_tm_params *params);
int tmtc_generate_ack(struct tmtc_telemetry *ack, struct tmtc_ack_params *params);

int tmtc_decode_tc_ignorecrc(struct tmtc_telecommand *tc);

int tmtc_encode_tc(struct tmtc_telecommand *tc, const struct tmtc_tc_params *tc_params);

#endif /* TMTCAPI_TMTC_H */
