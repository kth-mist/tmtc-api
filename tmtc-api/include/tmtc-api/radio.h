/**
 * @file radio.h
 *
 * @author Muhamad Andung Muntaha <muam@kth.se>
 * @author John Wikman
 * @author Erik Flink
 * @author Theodor Stana
 */

#ifndef TMTCAPI_RADIO_H
#define TMTCAPI_RADIO_H

#include <stdint.h>

struct tmtc_radio {
	/* @brief The I2C address of the radio receiver (radio uplink) */
	uint8_t i2c_rx_addr;

	/* @brief The I2C address of the radio transmitter (radio downlink) */
	uint8_t i2c_tx_addr;
};

int tmtc_radio_initialize(struct tmtc_radio *sim);
int tmtc_radio_index(void);
int tmtc_radio_soft_reset(void);
int tmtc_radio_hard_reset(void);

int tmtc_radio_set_beacon(uint8_t *src, size_t len, uint16_t interval);
int tmtc_radio_clear_beacon(void);
int tmtc_radio_set_to_callsign(uint8_t *callsign, uint8_t ssid);
int tmtc_radio_set_from_callsign(uint8_t *callsign, uint8_t ssid);
int tmtc_radio_set_tx_idle_state(uint8_t idle_state);
int tmtc_radio_set_tx_bitrate(uint16_t bitrate);

int tmtc_radio_tx(uint8_t *src, size_t len);
int tmtc_radio_rx(uint8_t *dst, size_t *len);

int tmtc_radio_rxbuf_frame_count(size_t *count);
uint8_t tmtc_radio_txbuf_last_avail_slots(void);

#endif /* TMTCAPI_RADIO_H */
