/**
 * @file ax25.h
 * @author Muhamad Andung Muntaha <muam@kth.se>
 * @author John Wikman
 * @author William Stackenäs
 *
 * @brief AX.25 packet generation matters definition
 */


#ifndef TMTCAPI_AX25_H
#define TMTCAPI_AX25_H

#include <stdint.h>
#include <stdlib.h>

#include "config/config.h"

#define AX25_TC_HEADER_SIZE (1)
#define AX25_TC_DATA_OFFSET (AX25_TC_HEADER_SIZE)

#define AX25_TM_HEADER_SIZE (4)
#define AX25_TM_DATA_OFFSET (AX25_TM_HEADER_SIZE)

#define AX25_TM_TRAILER_MINSIZE (1)
#define AX25_TM_TRAILER_MAXSIZE (5)

#define AX25_TC_ENCODED_MAXLENGTH (TMTC_TRXVU_UPLINK_PAYLOAD_MAXSIZE)
#define AX25_TM_ENCODED_MAXLENGTH (TMTC_TRXVU_DOWNLINK_PAYLOAD_MAXSIZE)

/**
 * @brief The fields in the information header of a AX.25 telecommand frame.
 *
 * Fields which have their values predetermined does not have an associated
 * member in this struct.
 */
struct ax25_tc_info {
	/* Telecommand Transfer Frame Secondary Header fields
	 * predetermined:
	 *  - spare = 0b000000
	 */
	uint8_t sequence_flag : 2;

	/* Data fields
	 *  - data: Must be provided separately
	 */
};

/**
 * @brief The fields in the information header of a v1 AX.25 telemetry frame.
 *
 * Fields which have their values predetermined does not have an associated
 * member in this struct.
 */
struct ax25_tm_info {
	/* Telemetry Transfer Frame Secondary Header fields
	 * predetermined:
	 *  - version_number = 0b00
	 *  - spare = 0b000
	 */
	uint8_t virtual_channel_id : 3;
	uint8_t master_frame_count;
	uint8_t virtual_channel_frame_count;
	uint8_t first_header_pointer;

	/* Data fields
	 *  - data: Must be provided separately
	 */

	/* Telemetry Transfer Frame Trailer fields
	 * predetermined:
	 *  - time_flag[1:3] = 0b000, if time_flag[0] = 0 or
	 *                     0b011, if time_flag[0] = 1
	 *  - spare = 0b00
	 */
	uint8_t time_flag : 1;
	uint8_t tc_count : 2;
	uint32_t time;
};

int ax25_encode_tc_info(uint8_t *dst, size_t *encsize, const struct ax25_tc_info *frame);
int ax25_decode_tc_info(struct ax25_tc_info *frame, const uint8_t *src);

int ax25_encode_tm_info_header(uint8_t *dst, size_t *encsize, const struct ax25_tm_info *frame);
int ax25_decode_tm_info_header(struct ax25_tm_info *frame, const uint8_t *src);

int ax25_encode_tm_info_trailer(uint8_t *dst, size_t *encsize, const struct ax25_tm_info *frame);
int ax25_decode_tm_info_trailer(struct ax25_tm_info *frame, const uint8_t *src);

#endif /* TMTCAPI_AX25_H */
