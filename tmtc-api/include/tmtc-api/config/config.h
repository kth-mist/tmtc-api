/*!
 * @file config.h
 * @author Muhamad Andung Muntaha <muam@kth.se>
 * @author John Wikman
 *
 * @brief Configuration file for tmtc-api
 */

#ifndef TMTCAPI_CONFIG_H
#define TMTCAPI_CONFIG_H

/**
 * @brief I2C address of TRXVU transmitter.
 *
 * Refer to TRXVU Option Sheet.
 */
#define TMTC_CONFIG_TRXVU_TX_I2C_ADDRESS (0x61)

/**
 * @brief I2C address of TRXVU receiver.
 */
#define TMTC_CONFIG_TRXVU_RX_I2C_ADDRESS (0x60)

/**
 * @brief Transmitter bitrate. Allowed values: 1200, 2400, 4800, 9600.
 *
 * Will default to 1200 if set to a non-allowed value.
 */
#define TMTC_CONFIG_TRXVU_BITRATE (1200)

/**
 * @brief The default index of the TRXVU.
 *
 * @warning DO NOT CHANGE THIS VALUE
 */
#define TMTC_CONFIG_TRXVU_INDEX (0)

/**
 * @brief The default index of the optional TRXVU simulator.
 *
 * @warning DO NOT CHANGE THIS VALUE
 */
#define TMTC_CONFIG_TRXVU_SIM_INDEX (1)

/**
 * @brief The AX.25 callsign used for the MIST satellite.
 */
#define TMTC_CONFIG_AX25_FROM_CALLSIGN "MIST  "

/**
 * @brief The AX.25 Secondary Station Identifier used for the MIST satellite.
 */
#define TMTC_CONFIG_AX25_FROM_SSID 0

/**
 * @brief The AX.25 callsign used for the MIST ground station.
 */
#define TMTC_CONFIG_AX25_TO_CALLSIGN "SA0SAT"

/**
 * @brief The AX.25 Secondary Station Identifier used for the MIST ground station.
 */
#define TMTC_CONFIG_AX25_TO_SSID 0

/**
 * @brief TRXVU watchdog timeout period.
 *
 * TRXVU needs to receive a command once every this period, otherwise it will
 * reboot.
 */
#define TMTC_CONFIG_TRXVU_WATCHDOG_TIMEOUT (60)

/**
 * @brief Delay in ms between I2C write and read in ISIS I2C_writeRead function.
 *
 * TODO: Why is this a configurable constant?
 */
#define TMTC_CONFIG_I2C_WRITE_READ_DELAY (20)

/**
 * @brief Maximum payload size for an uplink frame.
 *
 * Based on the ISIS TRXVU Option Sheet.
 *
 * For MIST, this corresponds to the size of the info field in an AX.25
 * telecommand frame.
 *
 * @warning DO NOT CHANGE THIS VALUE
 */
#define TMTC_TRXVU_UPLINK_PAYLOAD_MAXSIZE (200)

/**
 * @brief Maximum payload size for an downlink frame.
 *
 * Based on the ISIS TRXVU Option Sheet.
 *
 * For MIST, this corresponds to the size of the info field in an AX.25
 * telemetry frame.
 *
 * @warning DO NOT CHANGE THIS VALUE
 */
#define TMTC_TRXVU_DOWNLINK_PAYLOAD_MAXSIZE (235)

/**
 * @brief Maximum repeat interval for the beacon mode (seconds).
 *
 * Based on the ISIS TRXVU Interface Control Document.
 *
 * @warning DO NOT CHANGE THIS VALUE
 */
#define TMTC_TRXVU_BEACON_MAX_INTERVAL (3000)

#endif /* TMTCAPI_CONFIG_H */
