/*!
 * @file crc.h
 *
 * @brief CRC related function definition
 */

#ifndef TMTCAPI_CRC_H
#define TMTCAPI_CRC_H

#include <stdint.h>
#include <stdlib.h>

void tmtc_crc16_initialize_table(void);
int tmtc_crc16(const uint8_t *data, size_t length, uint16_t *chksum);
uint16_t tmtc_crc16_optimized(uint8_t data, uint16_t check);

#endif /* TMTCAPI_CRC_H */
