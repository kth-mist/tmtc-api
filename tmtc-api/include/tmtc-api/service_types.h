/**
 * @file tmtc_types.h
 * @author Muhamad Andung Muntaha <muam@kth.se>
 * @author John Wikman
 *
 * @brief tmtc-api standard service types
 */

#ifndef TMTCAPI_SERVICE_TYPES_H
#define TMTCAPI_SERVICE_TYPES_H

/* Service Type: Telemetry Acknowledgments */
#define TMTC_ST_ACK (1)
#define TMTC_ST_ACK_SST_ACCEPTANCE_SUCCESS          (1)
#define TMTC_ST_ACK_SST_ACCEPTANCE_FAIL             (2)
#define TMTC_ST_ACK_SST_EXECUTION_STARTED_SUCCESS   (3)
#define TMTC_ST_ACK_SST_EXECUTION_STARTED_FAIL      (4)
#define TMTC_ST_ACK_SST_EXECUTION_COMPLETED_SUCCESS (7)
#define TMTC_ST_ACK_SST_EXECUTION_COMPLETED_FAIL    (8)

#endif /* TMTCAPI_SERVICE_TYPES_H */
