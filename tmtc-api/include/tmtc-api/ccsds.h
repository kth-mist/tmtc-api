/**
 * @file ccsds.h
 * @author Muhamad Andung Muntaha <muam@kth.se>
 * @author John Wikman
 * @author William Stackenäs
 *
 * @brief CCSDS object and function definitions
 *
 * Modified from previous student implementation
 */

#ifndef TMTCAPI_CCSDS_H
#define TMTCAPI_CCSDS_H

#include <stdint.h>
#include <stdlib.h>

#include "ax25.h"

#define CCSDS_PACKET_HEADER_SIZE (6)
#define CCSDS_PACKET_ERROR_CONTROL_SIZE (2)
#define CCSDS_TC_PACKET_DATA_FIELD_METADATA_SIZE (5)
#define CCSDS_TM_PACKET_DATA_FIELD_METADATA_SIZE (10)

#define CCSDS_TC_APPLICATION_DATA_OFFSET (CCSDS_PACKET_HEADER_SIZE + CCSDS_TC_PACKET_DATA_FIELD_METADATA_SIZE - CCSDS_PACKET_ERROR_CONTROL_SIZE)
#define CCSDS_TM_SOURCE_DATA_OFFSET (CCSDS_PACKET_HEADER_SIZE + CCSDS_TM_PACKET_DATA_FIELD_METADATA_SIZE - CCSDS_PACKET_ERROR_CONTROL_SIZE)

#define CCSDS_TC_ENCODED_MAXLENGTH ((AX25_TC_ENCODED_MAXLENGTH) - (AX25_TC_HEADER_SIZE))
#define CCSDS_TM_ENCODED_MAXLENGTH ((AX25_TM_ENCODED_MAXLENGTH) - (AX25_TM_HEADER_SIZE + AX25_TM_TRAILER_MINSIZE))

#define CCSDS_TC_PACKET_LENGTH(appdata_len) ((appdata_len) + (CCSDS_TC_PACKET_DATA_FIELD_METADATA_SIZE - 1))
#define CCSDS_TM_PACKET_LENGTH(srcdata_len) ((srcdata_len) + (CCSDS_TM_PACKET_DATA_FIELD_METADATA_SIZE - 1))

#define CCSDS_TC_APPLICATION_DATA_LENGTH(packet_length) ((packet_length) - (CCSDS_TC_PACKET_DATA_FIELD_METADATA_SIZE - 1))
#define CCSDS_TM_SOURCE_DATA_LENGTH(packet_length) ((packet_length) - (CCSDS_TM_PACKET_DATA_FIELD_METADATA_SIZE - 1))

#define CCSDS_TC_ENCODED_LENGTH(packet) (((packet).packet_length) + 1 + (CCSDS_PACKET_HEADER_SIZE))
#define CCSDS_TM_ENCODED_LENGTH(packet) (((packet).packet_length) + 1 + (CCSDS_PACKET_HEADER_SIZE))

/**
 * @brief The fields of a v0 CCSDS telecommand packet.
 *
 * Fields which have their values predetermined does not have an associated
 * member in this struct.
 */
struct ccsds_tc {
	/* Packet ID fields
	 * predetermined:
	 *  - version_number = 0b000,
	 *  - type = 0b1,
	 *  - data_field_header_flag = 0b1
	 */
	uint16_t apid : 11;

	/* Packet Sequence Control fields
	 * predetermined:
	 *  - sequence_flags = 0b11 (only stand-alone packets)
	 */
	uint16_t sequence_count : 14;

	/* Remaining Packet Header fields */
	uint16_t packet_length;

	/* Telecommand Data Field Header "subfields"
	 * predetermined:
	 *  - ccsds_secondary_header_flag = 0b0
	 *  - tc_packet_pus_version_number = 0b001
	 */
	uint8_t ack : 4;
	uint8_t service_type;
	uint8_t service_subtype;

	/* Remaining Packet Data Field "subfields"
	 *  - application_data: must be handled separately
	 * predetermined:
	 *  - packet_error_control: derived from the other fields
	 */
};

/**
 * @brief The fields of a v0 CCSDS telemetry packet.
 *
 * Fields which have their values predetermined does not have an associated
 * member in this struct.
 */
struct ccsds_tm {
	/* Packet ID fields
	 * predetermined:
	 *  - version_number = 0b000,
	 *  - type = 0b1,
	 *  - data_field_header_flag = 0b1
	 */
	uint16_t apid : 11;

	/* Packet Sequence Control fields
	 * predetermined:
	 *  - grouping_flags = 0b11 (only stand-alone packets)
	 */
	uint16_t source_sequence_count : 14;

	/* Remaining Packet Header fields */
	uint16_t packet_length;

	/* Telemetry Data Field Header "subfields"
	 * predetermined:
	 *  - spare = 0b0
	 *  - tm_source_packet_pus_version_number = 0b001
	 *  - spare2 = 0b0000
	 */
	uint8_t service_type;
	uint8_t service_subtype;
	uint32_t time; /**< 32-bit Unix Epoch Timestamp, converted during encode */

	/* Remaining Packet Data Field "subfields"
	 *  - source_data: must be handled separately
	 * predetermined:
	 *  - packet_error_control: derived from the other fields
	 */
};

int ccsds_encode_tc(uint8_t *dst, size_t *encsize, const struct ccsds_tc *pkt, const uint8_t *application_data);
int ccsds_decode_tc(struct ccsds_tc *pkt, const uint8_t *src);
int ccsds_decode_tc_ignorecrc(struct ccsds_tc *pkt, const uint8_t *src);

int ccsds_encode_tm(uint8_t *dst, size_t *encsize, const struct ccsds_tm *pkt, const uint8_t *source_data);
int ccsds_decode_tm(struct ccsds_tm *pkt, const uint8_t *src);

#endif /* TMTCAPI_CCSDS_H */
