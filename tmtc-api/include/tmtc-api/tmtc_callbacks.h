/**
 * @file tmtc_callbacks.h
 * @author Erik Flink
 *
 * @brief Definitions for tmtc-api callback functions that should be implemented
 *        by the user of the API.
 */

#ifndef TMTCAPI_TMTC_CALLBACKS_H
#define TMTCAPI_TMTC_CALLBACKS_H

#include <stdint.h>

/**
 * @brief Callback function that, for a given APID,
 *        returns the current CCSDS sequence count
 *        and increments it.
 */
uint16_t tmtc_ccsds_sequence_count_cb(uint16_t apid);

/**
 * @brief Callback function that returns the
 *        current AX.25 master frame count and
 *        increments it.
 */
uint8_t tmtc_ax25_master_frame_count_cb(void);

/**
 * @brief Callback function that, for a given
 *        virtual channel, returns its current
 *        AX.25 virtual frame count and
 *        increments it.
 */
uint8_t tmtc_ax25_virtual_frame_count_cb(uint8_t virtual_id);

#endif /* TMTCAPI_TMTC_CALLBACKS_H */
