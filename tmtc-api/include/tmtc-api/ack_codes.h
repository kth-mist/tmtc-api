/**
 * @file ack_codes.h
 * @author Muhamad Andung Muntaha <muam@kth.se>
 * @author John Wikman
 *
 * @brief Predefined codes for telecommand acknowledgment.
 */

#ifndef TMTCAPI_ACK_CODES_H
#define TMTCAPI_ACK_CODES_H

#define TMTC_ACK_ERROR_OK                 (99)
#define TMTC_ACK_ERROR_ILLEGAL_APID       (0)
#define TMTC_ACK_ERROR_PACKET_INCOMPLETE  (1)
#define TMTC_ACK_ERROR_INCORRECT_CHECKSUM (2)
#define TMTC_ACK_ERROR_ILLEGAL_TYPE       (3)
#define TMTC_ACK_ERROR_ILLEGAL_SUBTYPE    (4)
#define TMTC_ACK_ERROR_ILLEGAL_DATA       (5)

#endif /* TMTCAPI_ACK_CODES_H */
