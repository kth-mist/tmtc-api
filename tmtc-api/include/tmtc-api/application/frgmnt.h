/**
 * @file    frgmnt.h
 * @author  Erik Flink
 *
 * Fragmentation of data blocks.
 *
 * Structure of fragments:
 * \code{.unparsed}
 *     |-----------------|
 *  0  |     BLOCK ID    | 1 byte
 *     |-----------------|
 *  1  | FRAGMENT NUMBER | 1 byte
 *     |-----------------|
 *  2  |    TAIL COUNT   | 1 byte
 *     |-----------------|
 *  3  |                 |
 *  4  |      DATA       |
 *  5  |                 | 0 to fragment_size - 3 bytes
 *     .                 .
 *
 *     .                 .
 *     |-----------------|
 * \endcode
 *
 * This implementation supports fragment sizes of up to 255 B.
 */

#ifndef FRGMNT_H
#define FRGMNT_H

#include <inttypes.h>

// TODO Define these using constraints present in the TMTC radio
#define FRGMNT_FRAGMENT_MAXSIZE (255)
#define FRGMNT_MAX_TAILCOUNT (255)

#define FRGMNT_BLOCK_MAXSIZE(fragment_size) ((1 + FRGMNT_MAX_TAILCOUNT) * (fragment_size - 3))


/**
 * A struct containing the current fragmentation state.
 */
typedef struct fragmenter {
	const uint8_t *block;
	uint16_t block_size;
	uint8_t block_id;
	uint8_t fragment_size;
	uint8_t fragment_number;
	uint8_t tail_count;
	uint16_t offset;
} fragmenter_t;

int frgmnt_init_fragmentation(fragmenter_t *f, const uint8_t *block, uint16_t block_size, uint8_t fragment_size, int *fragment_count);
int frgmnt_get_next_fragment(fragmenter_t *f, uint8_t *fragmentbuf, uint8_t *fragment_size);

#endif /* FRGMNT_H */
