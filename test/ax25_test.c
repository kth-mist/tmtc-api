
#include <tmtc-api/ax25.h>

#include <munit.h>

void test_ax25_tc_decoding_encoding(void)
{
	int err;
	struct ax25_tc_info tc;
	uint8_t buf[AX25_TC_ENCODED_MAXLENGTH];
	size_t encsize;

	munit_assert_int(AX25_TC_HEADER_SIZE, ==, 1);

	// Set sequence flag = 1: (0100 0000)
	buf[0] = 0x40;
	err = ax25_decode_tc_info(&tc, buf);
	munit_assert_int(err, ==, 0);
	munit_assert_uint8(tc.sequence_flag, ==, 1);

	// Set invalid spare (last 6 bits must be 0)
	buf[0] = 0x4F;
	err = ax25_decode_tc_info(&tc, buf);
	munit_assert_int(err, !=, 0);

	// Decode header
	tc.sequence_flag = 2;
	err = ax25_encode_tc_info(buf, &encsize, &tc);
	munit_assert_int(err, ==, 0);
	munit_assert_uint8(buf[0], ==, 0x80);
	munit_assert_size(encsize, ==, 1);
}

void test_ax25_tm_header_decoding_encoding(void)
{
	int err;
	struct ax25_tm_info tm;
	uint8_t buf[AX25_TM_ENCODED_MAXLENGTH];
	size_t encsize;

	buf[0] = 0x08; // virtual channel 1
	buf[1] = 0x07; // master frame count: 7
	buf[2] = 0x02; // virtual frame count: 2
	buf[3] = 0x09; // first header pointer: 9

	err = ax25_decode_tm_info_header(&tm, buf);
	munit_assert_int(err, ==, 0);
	munit_assert_uint8(tm.virtual_channel_id, ==, 1);
	munit_assert_uint8(tm.master_frame_count, ==, 7);
	munit_assert_uint8(tm.virtual_channel_frame_count, ==, 2);
	munit_assert_uint8(tm.first_header_pointer, ==, 9);

	// Set invalid version number (must be '00')
	buf[0] = 0xC8; // virtual channel 1
	buf[1] = 0x07; // master frame count: 7
	buf[2] = 0x02; // virtual frame count: 2
	buf[3] = 0x09; // first header pointer: 9
	err = ax25_decode_tm_info_header(&tm, buf);
	munit_assert_int(err, !=, 0);

	tm.virtual_channel_id = 0x7;
	tm.master_frame_count = 0x3F;
	tm.virtual_channel_frame_count = 0xF;
	tm.first_header_pointer = 0xFE;
	err = ax25_encode_tm_info_header(buf, &encsize, &tm);
	munit_assert_int(err, ==, 0);
	munit_assert_uint8(buf[0], ==, 0x38);
	munit_assert_uint8(buf[1], ==, 0x3F);
	munit_assert_uint8(buf[2], ==, 0x0F);
	munit_assert_uint8(buf[3], ==, 0xFE);
	munit_assert_size(encsize, ==, 4);
}

void test_ax25_tm_trailer_decoding_encoding(void)
{
	int err;
	struct ax25_tm_info tm;
	uint8_t buf[AX25_TM_ENCODED_MAXLENGTH];
	size_t encsize;

	buf[0] = 0x03; // time flag: 0, tc count = 3
	err = ax25_decode_tm_info_trailer(&tm, buf);
	munit_assert_int(err, ==, 0);
	munit_assert_uint8(tm.time_flag, ==, 0);
	munit_assert_uint8(tm.tc_count, ==, 3);

	buf[0] = 0xB2; // time flag: 1 (0b1011), tc count = 2
	buf[1] = 0x12;
	buf[2] = 0x37;
	buf[3] = 0x21;
	buf[4] = 0x73;
	err = ax25_decode_tm_info_trailer(&tm, buf);
	munit_assert_int(err, ==, 0);
	munit_assert_uint8(tm.time_flag, ==, 1);
	munit_assert_uint8(tm.tc_count, ==, 2);
	munit_assert_uint32(tm.time, ==, 0x12372173);

	buf[0] = 0x91; // invalid time flag (0b1001)
	err = ax25_decode_tm_info_trailer(&tm, buf);
	munit_assert_int(err, !=, 0);

	buf[0] = 0x31; // invalid time flag (0b011)
	err = ax25_decode_tm_info_trailer(&tm, buf);
	munit_assert_int(err, !=, 0);

	tm.time_flag = 0;
	tm.tc_count = 1;
	err = ax25_encode_tm_info_trailer(buf, &encsize, &tm);
	munit_assert_int(err, ==, 0);
	munit_assert_size(encsize, ==, 1);
	munit_assert_uint8(buf[0], ==, 0x01);

	tm.time_flag = 1;
	tm.tc_count = 0;
	tm.time = 0x243F6A88;
	err = ax25_encode_tm_info_trailer(buf, &encsize, &tm);
	munit_assert_int(err, ==, 0);
	munit_assert_size(encsize, ==, 5);
	munit_assert_uint8(buf[0], ==, 0xB0);
	munit_assert_uint8(buf[1], ==, 0x24);
	munit_assert_uint8(buf[2], ==, 0x3F);
	munit_assert_uint8(buf[3], ==, 0x6A);
	munit_assert_uint8(buf[4], ==, 0x88);
}
