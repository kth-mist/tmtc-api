/**
 * @file test_radio.c
 * @author Sonal Shrivastava
 * @author Erik Flink
 *
 * @brief Dummy stubs implementation emulating radio related functionality
 *
 * This file consists of functions acting as stubs to radio related functions that are part of ISIS-SDK. These are required to
 * make C compiler happy while building the tmtc-api pipeline.
 *
 */

#include <stdint.h>
#include <stddef.h>

#include <tmtc-api/radio.h>

int tmtc_radio_initialize(struct tmtc_radio *sim)
{
	return 0;
}

int tmtc_radio_index(void)
{
	return 0;
}

int tmtc_radio_soft_reset(void)
{
	return 0;
}

int tmtc_radio_hard_reset(void)
{
	return 0;
}

int tmtc_radio_set_beacon(uint8_t *src, size_t len, uint16_t interval)
{
	return 0;
}

int tmtc_radio_clear_beacon(void)
{
	return 0;
}

int tmtc_radio_set_to_callsign(uint8_t *callsign, uint8_t ssid)
{
	return 0;
}

int tmtc_radio_set_from_callsign(uint8_t *callsign, uint8_t ssid)
{
	return 0;
}

int tmtc_radio_set_tx_idle_state(uint8_t idle_state)
{
	return 0;
}

int tmtc_radio_set_tx_bitrate(uint16_t bitrate)
{
	return 0;
}

int tmtc_radio_tx(uint8_t *src, size_t len)
{
	return 0;
}

int tmtc_radio_rx(uint8_t *dst, size_t *len)
{
	return 0;
}

int tmtc_radio_rxbuf_frame_count(size_t *count)
{
	return 0;
}
