
#include <stdint.h>

#include <tmtc-api/ax25.h>
#include <tmtc-api/ccsds.h>
#include <tmtc-api/error.h>
#include <tmtc-api/tmtc.h>

#include <munit.h>

void test_tmtc_encode_tc(void)
{
	struct tmtc_tc_params tc_params;
	struct tmtc_telecommand tc;
	int err;
	uint8_t buf[2];
	uint8_t ack;

	//single complete packet
	tc_params.sequence_flag = 0x03;
	// XTEST experiment apid
	tc_params.apid = 179;
	// TC Seq count
	tc_params.tc_seq_count = 12;
	// appdatalen = 2 bytes allocated by default as per the macro struct
	tc_params.app_data_length = 2;
	// ACk type for TC
	tc_params.ack_acceptance = 0x01;
	tc_params.ack_exec_start = 0x01;
	tc_params.ack_exec_progress = 0x00;
	tc_params.ack_exec_completion = 0x01;
	// XTEST service type
	tc_params.service_type = 179;
	// XTEST service subtype
	tc_params.service_subtype = 1;
	// Set allocated data bytes to 0x02
	memset(&buf[0], 0x02, 2);
	tc_params.application_data = &buf[0];

	err = tmtc_encode_tc(&tc, &tc_params);

	munit_assert_int(err, ==, 0);
	munit_assert_uint8(tc.ax25.sequence_flag, ==, tc_params.sequence_flag);
	munit_assert_uint16(tc.ccsds.apid, ==, tc_params.apid);
	munit_assert_uint16(tc.ccsds.sequence_count, ==, tc_params.tc_seq_count);
	munit_assert_uint16(tc.ccsds.packet_length, ==, CCSDS_TC_PACKET_LENGTH(tc_params.app_data_length));
	ack = tc_params.ack_acceptance << 3 | tc_params.ack_exec_start << 2 | tc_params.ack_exec_progress << 1 | tc_params.ack_exec_completion;
	munit_assert_uint8(tc.ccsds.ack, ==, ack);
	munit_assert_uint8(tc.ccsds.service_type, ==, tc_params.service_type);
	munit_assert_uint8(tc.ccsds.service_subtype, ==, tc_params.service_subtype);
	munit_assert_int(memcmp(TC_CCSDS_APPLICATION_DATA(tc.rawdata), tc_params.application_data, tc_params.app_data_length), ==, 0);
	munit_assert_uint64(tc.rawlength, ==, AX25_TC_HEADER_SIZE + CCSDS_TC_ENCODED_LENGTH(tc.ccsds));
}
