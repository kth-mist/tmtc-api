// Test Suite main file

#include <munit.h>

#include "ax25_test.h"
#include "crc_test.h"
#include "ccsds_test.h"
#include "tmtc_encode_tc_test.h"

#include "application/frgmnt_test.h"

MunitResult crc_compl(const MunitParameter params[], void* user_data)
{
	crc_compliance_verification();

	return MUNIT_OK;
}

MunitResult ax25_tc_encdec(const MunitParameter params[], void* user_data)
{
	test_ax25_tc_decoding_encoding();

	return MUNIT_OK;
}

MunitResult ax25_tm_header_encdec(const MunitParameter params[], void* user_data)
{
	test_ax25_tm_header_decoding_encoding();

	return MUNIT_OK;
}

MunitResult ax25_tm_trailer_encdec(const MunitParameter params[], void* user_data)
{
	test_ax25_tm_trailer_decoding_encoding();

	return MUNIT_OK;
}

MunitResult ccsds_tc_encdec(const MunitParameter params[], void* user_data)
{
	test_ccsds_tc_decoding_encoding();

	return MUNIT_OK;
}

MunitResult ccsds_tm_encdec(const MunitParameter params[], void* user_data)
{
	test_ccsds_tm_decoding_encoding();

	return MUNIT_OK;
}

MunitResult tmtc_enc_tc(const MunitParameter params[], void* user_data)
{
	test_tmtc_encode_tc();

	return MUNIT_OK;
}

MunitResult fragmentation_arguments(const MunitParameter params[], void* user_data)
{
	test_fragmentation_arguments();

	return MUNIT_OK;
}

MunitResult fragmentation_defragmentation(const MunitParameter params[], void* user_data)
{
	test_fragmentation_defragmentation();

	return MUNIT_OK;
}

static MunitTest tests[] =
{
	{
		"/crc-compliance",
		crc_compl,
		NULL,
		NULL,
		MUNIT_TEST_OPTION_NONE,
		NULL
	},
	{
		"/ax25-tc",
		ax25_tc_encdec,
		NULL,
		NULL,
		MUNIT_TEST_OPTION_NONE,
		NULL
	},
	{
		"/ax25-tm-header",
		ax25_tm_header_encdec,
		NULL,
		NULL,
		MUNIT_TEST_OPTION_NONE,
		NULL
	},
	{
		"/ax25-tm-trailer",
		ax25_tm_trailer_encdec,
		NULL,
		NULL,
		MUNIT_TEST_OPTION_NONE,
		NULL
	},
	{
		"/ccsds-tc",
		ccsds_tc_encdec,
		NULL,
		NULL,
		MUNIT_TEST_OPTION_NONE,
		NULL
	},
	{
		"/ccsds-tm",
		ccsds_tm_encdec,
		NULL,
		NULL,
		MUNIT_TEST_OPTION_NONE,
		NULL
	},
	{
		"/tmtc-encode",
		tmtc_enc_tc,
		NULL,
		NULL,
		MUNIT_TEST_OPTION_NONE,
		NULL
	},
	{
		"/application/frgmnt-arguments",
		fragmentation_arguments,
		NULL,
		NULL,
		MUNIT_TEST_OPTION_NONE,
		NULL
	},
	{
		"/application/frgmnt-dfrgmnt",
		fragmentation_defragmentation,
		NULL,
		NULL,
		MUNIT_TEST_OPTION_NONE,
		NULL
	},
	{
		NULL,
		NULL,
		NULL,
		NULL,
		MUNIT_TEST_OPTION_NONE,
		NULL
	}
};

static const MunitSuite test_suite =
{
	"/packet-generation",
	tests,
	NULL,
	1,
	MUNIT_SUITE_OPTION_NONE
};

int main (int argc, char* argv[])
{
	munit_suite_main(&test_suite, NULL, argc, argv);

	return 0;
}
