
#include <string.h>

#include <tmtc-api/ccsds.h>
#include <tmtc-api/crc.h>

#include <munit.h>

void test_ccsds_tc_decoding_encoding(void)
{
	int err;
	struct ccsds_tc pkt;
	uint8_t buf[CCSDS_TC_ENCODED_MAXLENGTH];
	size_t encsize;
	uint16_t check;

	// APID=513
	buf[0] = 0x18 | 0x01;
	buf[1] = 0x01;
	// sequence count=516
	buf[2] = 0xC0 | 0x01;
	buf[3] = 0x04;
	// packet length=16 (=> length(appdata)=12)
	buf[4] = 0x00;
	buf[5] = 0x10;

	buf[6] = 0x10; // flag=0, version=1, ack=0
	buf[7] = 0x81; // service type
	buf[8] = 0xA5; // service subtype
	memset(&buf[9], 0x11, 12); // Set all data bytes to 0x11

	err = tmtc_crc16(buf, 9 + 12, &check);
	munit_assert_int(err, ==, 0);
	buf[9 + 12 + 0] = (check >> 8) & 0xFF;
	buf[9 + 12 + 1] = check & 0xFF;

	err = ccsds_decode_tc(&pkt, buf);
	munit_assert_int(err, ==, 0);
	munit_assert_uint16(pkt.apid, ==, 0x101);
	munit_assert_uint16(pkt.sequence_count, ==, 0x104);
	munit_assert_uint16(pkt.packet_length, ==, 16);
	munit_assert_uint16(CCSDS_TC_APPLICATION_DATA_LENGTH(pkt.packet_length), ==, 12);
	munit_assert_uint8(pkt.ack, ==, 0);
	munit_assert_uint8(pkt.service_type, ==, 0x81);
	munit_assert_uint8(pkt.service_subtype, ==, 0xA5);

	munit_assert_uint(9 + 12 + 2, ==, CCSDS_TC_ENCODED_LENGTH(pkt));

	// Same as above, but trigger CRC mismatch
	buf[8] ^= 0x40; // corrupt data
	err = ccsds_decode_tc(&pkt, buf);
	munit_assert_int(err, !=, 0);
	// Check that the ignore CRC variant decodes correctly instead
	err = ccsds_decode_tc_ignorecrc(&pkt, buf);
	munit_assert_int(err, ==, 0);
	munit_assert_uint16(pkt.apid, ==, 0x101);
	munit_assert_uint16(pkt.sequence_count, ==, 0x104);
	munit_assert_uint16(pkt.packet_length, ==, 16);
	munit_assert_uint16(CCSDS_TC_APPLICATION_DATA_LENGTH(pkt.packet_length), ==, 12);
	munit_assert_uint8(pkt.ack, ==, 0);
	munit_assert_uint8(pkt.service_type, ==, 0x81);
	munit_assert_uint8(pkt.service_subtype, ==, 0xA5 ^ 0x40);

	munit_assert_uint(9 + 12 + 2, ==, CCSDS_TC_ENCODED_LENGTH(pkt));

	buf[8] ^= 0x40; // un-corrupt data

	// Set version number to 1 and recalculate CRC (decode should fail)
	buf[0] |= 0x20;
	err = tmtc_crc16(buf, 9 + 12, &check);
	munit_assert_int(err, ==, 0);
	buf[9 + 12 + 0] = (check >> 8) & 0xFF;
	buf[9 + 12 + 1] = check & 0xFF;

	err = ccsds_decode_tc(&pkt, buf);
	munit_assert_int(err, !=, 0);

	// Encode a TC packet
	uint8_t mydata[] = {0x11, 0x22, 0x33};
	pkt.apid = 0xF3;
	pkt.sequence_count = 0x02;
	pkt.packet_length = CCSDS_TC_PACKET_LENGTH(3); // 3 bytes of data
	pkt.ack = 0; // no ack
	pkt.service_type = 0x01;
	pkt.service_subtype = 0x49;

	munit_assert_uint16(CCSDS_TC_PACKET_LENGTH(3), ==, 7);

	err = ccsds_encode_tc(buf, &encsize, &pkt, mydata);
	munit_assert_int(err, ==, 0);
	munit_assert_size(encsize, ==, 14);
	munit_assert_uint8(buf[0], ==, 0x18);
	munit_assert_uint8(buf[1], ==, 0xF3); // APID
	munit_assert_uint8(buf[2], ==, 0xC0);
	munit_assert_uint8(buf[3], ==, 0x02); // sequence count
	munit_assert_uint8(buf[4], ==, 0x00); // packet length MSB
	munit_assert_uint8(buf[5], ==, 0x07); // packet length LSB
	munit_assert_uint8(buf[6], ==, 0x10);
	munit_assert_uint8(buf[7], ==, 0x01); // service type
	munit_assert_uint8(buf[8], ==, 0x49); // service subtype
	munit_assert_uint8(buf[9], ==, mydata[0]);
	munit_assert_uint8(buf[10], ==, mydata[1]);
	munit_assert_uint8(buf[11], ==, mydata[2]);

	// Check that the crc was encoded
	err = tmtc_crc16(buf, 12, &check);
	munit_assert_int(err, ==, 0);
	munit_assert_uint8(buf[12], ==, (check >> 8) & 0xFF);
	munit_assert_uint8(buf[13], ==, check & 0xFF);
}


void test_ccsds_tm_decoding_encoding(void)
{
	int err;
	struct ccsds_tm pkt;
	uint8_t buf[CCSDS_TM_ENCODED_MAXLENGTH];
	size_t encsize;
	uint16_t check;

	// APID=0x206
	buf[0] = 0x08 | 0x02;
	buf[1] = 0x06;
	// source sequence count=0x108
	buf[2] = 0xC0 | 0x01;
	buf[3] = 0x08;
	// packet length=17 (length(srcdata)=8)
	buf[4] = 0x00;
	buf[5] = 0x11;
	// Telemetry data field header
	buf[6] = 0x10;
	buf[7] = 0x23; // service type
	buf[8] = 0x58; // service subtype
	// time=0x12345678
	buf[9] = 0x12;
	buf[10] = 0x34;
	buf[11] = 0x56;
	buf[12] = 0x78;
	buf[13] = 0x00;
	memset(&buf[14], 0x71, 8); // Set all data bytes to 0x71

	err = tmtc_crc16(buf, 14 + 8, &check);
	munit_assert_int(err, ==, 0);
	buf[14 + 8 + 0] = (check >> 8) & 0xFF;
	buf[14 + 8 + 1] = check & 0xFF;

	err = ccsds_decode_tm(&pkt, buf);
	munit_assert_int(err, ==, 0);
	munit_assert_uint16(pkt.apid, ==, 0x206);
	munit_assert_uint16(pkt.source_sequence_count, ==, 0x108);
	munit_assert_uint16(pkt.packet_length, ==, 17);
	munit_assert_uint16(CCSDS_TM_SOURCE_DATA_LENGTH(pkt.packet_length), ==, 8);
	munit_assert_uint8(pkt.service_type, ==, 0x23);
	munit_assert_uint8(pkt.service_subtype, ==, 0x58);
	munit_assert_uint32(pkt.time, ==, 0x12345678);

	munit_assert_uint(14 + 8 + 2, ==, CCSDS_TM_ENCODED_LENGTH(pkt));

	// Same as above, but trigger CRC mismatch
	buf[8] ^= 0x40; // corrupt data
	err = ccsds_decode_tm(&pkt, buf);
	munit_assert_int(err, !=, 0);

	buf[8] ^= 0x40; // un-corrupt data


	// Set version number to 1 and recalculate CRC (decode should fail)
	buf[0] |= 0x20;
	err = tmtc_crc16(buf, 14 + 8, &check);
	munit_assert_int(err, ==, 0);
	buf[14 + 8 + 0] = (check >> 8) & 0xFF;
	buf[14 + 8 + 1] = check & 0xFF;

	err = ccsds_decode_tm(&pkt, buf);
	munit_assert_int(err, !=, 0);

	// Encode a TM packet
	uint8_t mydata[] = {0x71, 0x17};
	pkt.apid = 0xAA;
	pkt.source_sequence_count = 0x110;
	pkt.packet_length = CCSDS_TM_PACKET_LENGTH(2); // 2 bytes of data
	pkt.service_type = 0x13;
	pkt.service_subtype = 0x17;
	pkt.time = 0x22114499;

	munit_assert_uint16(CCSDS_TM_PACKET_LENGTH(2), ==, 11);

	err = ccsds_encode_tm(buf, &encsize, &pkt, mydata);
	munit_assert_int(err, ==, 0);
	munit_assert_size(encsize, ==, 18);
	munit_assert_uint8(buf[0], ==, 0x08);
	munit_assert_uint8(buf[1], ==, 0xAA); // APID
	munit_assert_uint8(buf[2], ==, 0xC1); // groupflags+source sequence count (MSB)
	munit_assert_uint8(buf[3], ==, 0x10); // source sequence count (LSB)
	munit_assert_uint8(buf[4], ==, 0x00);
	munit_assert_uint8(buf[5], ==, 0x0B); // packet length=11
	munit_assert_uint8(buf[6], ==, 0x10);
	munit_assert_uint8(buf[7], ==, 0x13); // service type
	munit_assert_uint8(buf[8], ==, 0x17); // service subtype
	munit_assert_uint8(buf[9], ==, 0x22); // time=0x22114499 (PTC=9 PFC=16)
	munit_assert_uint8(buf[10], ==, 0x11);
	munit_assert_uint8(buf[11], ==, 0x44);
	munit_assert_uint8(buf[12], ==, 0x99);
	munit_assert_uint8(buf[13], ==, 0x00);
	munit_assert_uint8(buf[14], ==, mydata[0]);
	munit_assert_uint8(buf[15], ==, mydata[1]);

	// Check that the crc was encoded
	err = tmtc_crc16(buf, 16, &check);
	munit_assert_int(err, ==, 0);
	munit_assert_uint8(buf[16], ==, (check >> 8) & 0xFF);
	munit_assert_uint8(buf[17], ==, check & 0xFF);
}
