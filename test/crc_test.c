
#include <tmtc-api/crc.h>

#include <munit.h>

/**
 * Tests CCSDS CRC compliance from EECS-E-70-41A (2003-01-30):
 *
 * 00 00 -> 1D 0F
 * 00 00 00 -> CC 9C
 * AB CD EF 01 -> 04 A2
 * 14 56 F8 9A 00 01 -> 7F D5
 */
void crc_compliance_verification(void)
{
	int err;
	uint16_t check;

	uint8_t cpmltest1[] = {0x00, 0x00};
	uint8_t cpmltest2[] = {0x00, 0x00, 0x00};
	uint8_t cpmltest3[] = {0xAB, 0xCD, 0xEF, 0x01};
	uint8_t cpmltest4[] = {0x14, 0x56, 0xF8, 0x9A, 0x00, 0x01};

	err = tmtc_crc16(cpmltest1, sizeof(cpmltest1) / sizeof(cpmltest1[0]), &check);
	munit_assert_int(err, ==, 0);
	munit_assert_uint16(check, ==, 0x1D0F);

	err = tmtc_crc16(cpmltest2, sizeof(cpmltest2) / sizeof(cpmltest2[0]), &check);
	munit_assert_int(err, ==, 0);
	munit_assert_uint16(check, ==, 0xCC9C);

	err = tmtc_crc16(cpmltest3, sizeof(cpmltest3) / sizeof(cpmltest3[0]), &check);
	munit_assert_int(err, ==, 0);
	munit_assert_uint16(check, ==, 0x04A2);

	err = tmtc_crc16(cpmltest4, sizeof(cpmltest4) / sizeof(cpmltest4[0]), &check);
	munit_assert_int(err, ==, 0);
	munit_assert_uint16(check, ==, 0x7FD5);
}
