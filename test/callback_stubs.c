#include <tmtc-api/tmtc_callbacks.h>

// Stub callback functions needed by tmtc-api
uint16_t tmtc_ccsds_sequence_count_cb(uint16_t apid) { return 0; }
uint8_t tmtc_ax25_master_frame_count_cb(void) { return 0; }
uint8_t tmtc_ax25_virtual_frame_count_cb(uint8_t virtual_id) { return 0; }