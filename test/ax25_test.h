#ifndef AX25_TEST_H_
#define AX25_TEST_H_

/* void test_ax25_branch_coverage(); */

void test_ax25_tc_decoding_encoding(void);
void test_ax25_tm_header_decoding_encoding(void);
void test_ax25_tm_trailer_decoding_encoding(void);

#endif /* AX25_TEST_H_ */
