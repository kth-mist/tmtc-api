/**
 * @file    dfrgmnt.h
 * @author  Erik Flink
 *
 * Defragmentation of data blocks.
 */

#ifndef DFRGMNT_H
#define DFRGMNT_H

#include <inttypes.h>
#include <tmtc-api/application/frgmnt.h>

/**
 * A struct containing the current defragmentation state.
 */
typedef struct defragmenter {
	uint8_t *block;
	uint16_t block_maxsize;
	uint16_t block_size;
	uint8_t block_id;
	uint8_t fragment_size;
	uint8_t fragment_number;
	uint8_t tail_count;
	uint16_t offset;
	uint8_t missing_fragments;
} defragmenter_t;

int frgmnt_init_defragmentation(defragmenter_t *df, uint8_t *blockbuf, uint16_t size);
int frgmnt_put_fragment(defragmenter_t *df, uint8_t *fragment, uint8_t size, uint16_t *finalized_block_size);
uint8_t *frgmnt_get_block(defragmenter_t *df);
uint16_t frgmnt_get_block_size(defragmenter_t *df);
uint16_t frgmnt_get_missing_bytes(defragmenter_t *df);

#endif /* DFRGMNT_H */
