/**
 * @file    dfrgmnt.c
 * @author  Erik Flink
 *
 * Defragmentation of data blocks.
 * This implementation is only to be used for unit testing.
 */

#include <string.h>
#include <inttypes.h>
#include <stddef.h>

#include <tmtc-api/application/frgmnt.h>
#include <tmtc-api/error.h>

#include "dfrgmnt.h"

/**
 * Initializes a defragmenter.
 * @param df       Pointer to a defragmenter, for storage of the defragmentation state.
 * @param blockbuf Pointer to a buffer where defragmented blocks will be put.
 * @param size     The size of the buffer.
 *
 * @return 0 on success. Otherwise an error code from error.h
 */
int frgmnt_init_defragmentation(defragmenter_t *df, uint8_t *blockbuf, uint16_t size)
{
	if (df == NULL || blockbuf == NULL)
	{
		return TMTC_ERR_NULL_ARGUMENT;
	}

	df->block = blockbuf;
	df->block_maxsize = size < FRGMNT_BLOCK_MAXSIZE(FRGMNT_FRAGMENT_MAXSIZE) ?
	                    size : FRGMNT_BLOCK_MAXSIZE(FRGMNT_FRAGMENT_MAXSIZE);
	return 0;
}

/**
 * Processes a received fragment.
 * @param df                    Pointer to a defragmenter that has been initialized.
 * @param fragment              Pointer to the received fragment.
 * @param size                  The size of the received fragment.
 * @param finalized_block_size The number of bytes that are ready to be read from the block buffer.
 *
 * @return 0 on success. Otherwise an error code from error.h
 */
int frgmnt_put_fragment(defragmenter_t *df, uint8_t *fragment, uint8_t size, uint16_t *finalized_block_size)
{
	if (df == NULL || fragment == NULL || finalized_block_size == NULL)
	{
		return TMTC_ERR_NULL_ARGUMENT;
	}

	if (df->block == NULL)
	{
		return TMTC_ERR_NOT_INITIALIZED;
	}

	*finalized_block_size = 0;

	if ((fragment[1] == 0) ||
		(fragment[0] != df->block_id) ||                               // Other ID
		(fragment[1] <= df->fragment_number) ||                        // Lower number
		(fragment[2] != df->tail_count) ||                             // Other tail count
		(size != df->fragment_size && fragment[1] != df->tail_count))  // Other size and not last fragment
	{
		df->block_id = fragment[0];
		df->fragment_number = fragment[1];
		df->tail_count = fragment[2];
		df->fragment_size = size;
		df->block_size = (df->tail_count + 1) * (df->fragment_size - 3);
		if (df->block_size > df->block_maxsize)
		{
			df->block_size = df->block_maxsize;
		}

		for (int i = 0; i < df->block_size; i++)
		{
			df->block[i] = 0;
		}

		df->offset = 0;
		df->missing_fragments = df->tail_count;
	}
	else
	{
		df->fragment_number = fragment[1];
		df->offset = df->fragment_number * (df->fragment_size - 3);
		df->missing_fragments--;
	}
	int i = 3;

	while (i < size && df->offset < df->block_size)
	{
		df->block[df->offset++] = fragment[i++];
	}

	if (df->fragment_number == df->tail_count)
	{
		df->block_size = df->offset;
		*finalized_block_size = df->block_size;
	}

	return 0;
}

/**
 * Gets a pointer to the received block.
 * @param df Pointer to a defragmenter that has been initialized.
 *
 * @return A pointer to the block buffer of the defragmenter, if the fragmenter has been initialized.
 * Otherwise NULL.
 */
uint8_t *frgmnt_get_block(defragmenter_t *df)
{
	if (df == NULL)
	{
		return NULL;
	}

	return df->block;
}

/**
 * Gets the size of the block that has been received, or that is currently beeing received.
 * @param df Pointer to a defragmenter that has been initialized.
 *
 * @return The size of the block that has been received, or that is currently beeing received.
 */
uint16_t frgmnt_get_block_size(defragmenter_t *df)
{
	if (df == NULL)
	{
		return 0;
	}

	return df->block_size;
}

/**
 * Gets the number of bytes that are missing in the block that is being received.
 * @param df Pointer to a defragmenter that has been initialized.
 *
 * @return The number of bytes that are missing in the block that is being received.
 */
uint16_t frgmnt_get_missing_bytes(defragmenter_t *df)
{
	if (df == NULL)
	{
		return 0;
	}

	return df->missing_fragments * (df->fragment_size - 3);
}
