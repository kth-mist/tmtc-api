#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>

#include <munit.h>

#include <tmtc-api/application/frgmnt.h>
#include <tmtc-api/error.h>

#include "dfrgmnt.h"
#include "frgmnt_test.h"

static uint8_t inbuf[FRGMNT_BLOCK_MAXSIZE(FRGMNT_FRAGMENT_MAXSIZE)];
static uint8_t fragmentbuf[FRGMNT_FRAGMENT_MAXSIZE];
static uint8_t outbuf[FRGMNT_BLOCK_MAXSIZE(FRGMNT_FRAGMENT_MAXSIZE)];

fragmenter_t fragmenter;
defragmenter_t defragmenter;

void fill_inbuf()
{
	for (int i = 0; i < FRGMNT_BLOCK_MAXSIZE(FRGMNT_FRAGMENT_MAXSIZE); i++)
	{
		inbuf[i] = i % 256;
	}
}

void clear_outbuf()
{
	for (int i = 0; i < FRGMNT_BLOCK_MAXSIZE(FRGMNT_FRAGMENT_MAXSIZE); i++)
	{
		outbuf[i] = 0;
	}
}

void run_fragmentation_defragmentation(uint16_t block_size, uint8_t fragment_size)
{
	int ret;
	uint16_t final_block_size;
	int fragments;

	clear_outbuf();

	ret = frgmnt_init_fragmentation(&fragmenter, inbuf, block_size, fragment_size, &fragments);
	munit_assert_int(ret, ==, 0);
	munit_assert_int(fragments, >, 0);
	for (int i = 0; i < fragments; i++)
	{
		uint8_t fragment_size;
		ret = frgmnt_get_next_fragment(&fragmenter, fragmentbuf, &fragment_size);
		munit_assert_int(ret, ==, 0);
		munit_assert_uint8(fragment_size, >, 0);
		ret = frgmnt_put_fragment(&defragmenter, fragmentbuf, fragment_size, &final_block_size);
		munit_assert_int(ret, ==, 0);
		if (i == fragments - 1)
		{
			munit_assert_uint16(final_block_size, ==, block_size);
		}
		else
		{
			munit_assert_uint16(final_block_size, ==, 0);
		}
	}
	uint8_t *block = frgmnt_get_block(&defragmenter);
	munit_assert_ptr(block, ==, outbuf);
	uint16_t size = frgmnt_get_block_size(&defragmenter);
	munit_assert_uint16(size, ==, block_size);
	ret = frgmnt_get_missing_bytes(&defragmenter);
	munit_assert_uint16(ret, ==, 0);
	for (size_t i = 0; i < block_size; i++)
	{
		munit_assert_uint8(inbuf[i], ==, outbuf[i]);
	}
}

void test_fragmentation_arguments(void)
{
	int ret;
	uint16_t final_block_size;
	int fragments;
	uint8_t fragment_size;

	fill_inbuf();

	ret = frgmnt_put_fragment(&defragmenter, fragmentbuf, 0, &final_block_size);
	munit_assert_int(ret, ==, TMTC_ERR_NOT_INITIALIZED);

	ret = frgmnt_get_next_fragment(&fragmenter, fragmentbuf, &fragment_size);
	munit_assert_int(ret, ==, TMTC_ERR_NOT_INITIALIZED);

	ret = frgmnt_init_fragmentation(&fragmenter, inbuf, FRGMNT_BLOCK_MAXSIZE(10) + 1, 10, &fragments);
	munit_assert_int(ret, ==, TMTC_ERR_SIZE);

	ret = frgmnt_init_fragmentation(&fragmenter, inbuf, 10, 2, &fragments);
	munit_assert_int(ret, ==, TMTC_ERR_SIZE);

	ret = frgmnt_init_fragmentation(NULL, inbuf, 10, 10, &fragments);
	munit_assert_int(ret, ==, TMTC_ERR_NULL_ARGUMENT);

	ret = frgmnt_init_fragmentation(&fragmenter, NULL, 10, 10, &fragments);
	munit_assert_int(ret, ==, TMTC_ERR_NULL_ARGUMENT);

	ret = frgmnt_init_fragmentation(&fragmenter, inbuf, 10, 10, NULL);
	munit_assert_int(ret, ==, TMTC_ERR_NULL_ARGUMENT);

	ret = frgmnt_init_fragmentation(&fragmenter, inbuf, 10, 10, &fragments);
	munit_assert_int(ret, ==, 0);
	munit_assert_int(fragments, ==, 2);

	ret = frgmnt_get_next_fragment(NULL, fragmentbuf, &fragment_size);
	munit_assert_int(ret, ==, TMTC_ERR_NULL_ARGUMENT);

	ret = frgmnt_get_next_fragment(&fragmenter, NULL, &fragment_size);
	munit_assert_int(ret, ==, TMTC_ERR_NULL_ARGUMENT);

	ret = frgmnt_get_next_fragment(&fragmenter, fragmentbuf, NULL);
	munit_assert_int(ret, ==, TMTC_ERR_NULL_ARGUMENT);

	ret = frgmnt_init_defragmentation(NULL, outbuf, FRGMNT_BLOCK_MAXSIZE(FRGMNT_FRAGMENT_MAXSIZE));
	munit_assert_int(ret, ==, TMTC_ERR_NULL_ARGUMENT);

	ret = frgmnt_init_defragmentation(&defragmenter, NULL, FRGMNT_BLOCK_MAXSIZE(FRGMNT_FRAGMENT_MAXSIZE));
	munit_assert_int(ret, ==, TMTC_ERR_NULL_ARGUMENT);

	ret = frgmnt_init_defragmentation(&defragmenter, outbuf, FRGMNT_BLOCK_MAXSIZE(FRGMNT_FRAGMENT_MAXSIZE));
	munit_assert_int(ret, ==, 0);

	ret = frgmnt_put_fragment(NULL, fragmentbuf, 10, &final_block_size);
	munit_assert_int(ret, ==, TMTC_ERR_NULL_ARGUMENT);

	ret = frgmnt_put_fragment(&defragmenter, NULL, 10, &final_block_size);
	munit_assert_int(ret, ==, TMTC_ERR_NULL_ARGUMENT);

	ret = frgmnt_put_fragment(&defragmenter, fragmentbuf, 10, NULL);
	munit_assert_int(ret, ==, TMTC_ERR_NULL_ARGUMENT);

	uint8_t *block = frgmnt_get_block(NULL);
	munit_assert_ptr(block, ==, NULL);

	uint16_t size = frgmnt_get_block_size(NULL);
	munit_assert_uint16(size, ==, 0);

	ret = frgmnt_get_missing_bytes(NULL);
	munit_assert_int(ret, ==, 0);
}

void test_fragmentation_defragmentation(void)
{
	fill_inbuf();

	frgmnt_init_defragmentation(&defragmenter, outbuf, FRGMNT_BLOCK_MAXSIZE(FRGMNT_FRAGMENT_MAXSIZE));

	run_fragmentation_defragmentation(0, 10);
	run_fragmentation_defragmentation(10, 10);
	run_fragmentation_defragmentation(255, 255);
	run_fragmentation_defragmentation(8192, 214);
	run_fragmentation_defragmentation(FRGMNT_BLOCK_MAXSIZE(FRGMNT_FRAGMENT_MAXSIZE), FRGMNT_FRAGMENT_MAXSIZE);
}
