
#ifndef FRGMNT_TEST_H_
#define FRGMNT_TEST_H_

void test_fragmentation_arguments(void);
void test_fragmentation_defragmentation(void);

#endif /* FRGMNT_TEST_H_ */
