#include <freertos/FreeRTOS.h>
#include <freertos/semphr.h>
#include <freertos/task.h>

#include <hal/Timing/WatchDogTimer.h>
#include <hal/boolean.h>
#include <hal/Drivers/I2C.h>
#include <hal/Drivers/LED.h>
#include <hal/Drivers/SPI.h>
#include <hal/Storage/FRAM.h>
#include <hal/Timing/Time.h>
#include <hal/Utility/util.h>
#include <hal/version/version.h>

#include <at91/utility/trace.h>
#include <at91/utility/exithandler.h>
#include <at91/utility/exithandler.h>
#include <at91/commons.h>
#include <at91/peripherals/cp15/cp15.h>

#include <stdlib.h>
#include <stdio.h>

#include <tmtc-api/error.h>
#include <tmtc-api/tmtc.h>
#include <tmtc-api/tmtc_callbacks.h>

#define ENABLE_MAIN_TRACES 1
#if ENABLE_MAIN_TRACES
#define MAIN_TRACE_INFO			TRACE_INFO
#define MAIN_TRACE_DEBUG		TRACE_DEBUG
#define MAIN_TRACE_WARNING		TRACE_WARNING
#define MAIN_TRACE_ERROR		TRACE_ERROR
#define MAIN_TRACE_FATAL		TRACE_FATAL
#else
#define MAIN_TRACE_INFO(...)	{ }
#define MAIN_TRACE_DEBUG(...)	{ }
#define MAIN_TRACE_WARNING(...)	{ }
#define MAIN_TRACE_ERROR		TRACE_ERROR
#define MAIN_TRACE_FATAL		TRACE_FATAL
#endif

#define I2C_BIT_RATE 400000
#define I2C_TIMEOUT_RATE 10000

uint16_t tmtc_ccsds_sequence_count_cb(uint16_t apid)
{
    static uint16_t ccsds_psc_counter_apid_eps = 0x00;
    static uint16_t ccsds_psc_counter_apid_com = 0x00;
    static uint16_t ccsds_psc_counter_apid_cdms = 0x00;
    static uint16_t ccsds_psc_counter_apid_adcs = 0x00;
    static uint16_t ccsds_psc_counter_apid_pl = 0x00;
    static uint16_t ccsds_psc_counter_apid_beac = 0x00;

    switch (apid) {
    case 8: //TMTC_APID_EPS:
        return ccsds_psc_counter_apid_eps++;
    case 9: //TMTC_APID_COM:
        return ccsds_psc_counter_apid_com++;
    case 10: //TMTC_APID_CDMS:
        return ccsds_psc_counter_apid_cdms++;
    case 11: //TMTC_APID_ADCS:
        return ccsds_psc_counter_apid_adcs++;
    case 12: //TMTC_APID_PL:
        return ccsds_psc_counter_apid_pl++;
    case 13: //TMTC_APID_BEAC:
        return ccsds_psc_counter_apid_beac++;
    default:
        return 0;
    }
}

uint8_t tmtc_ax25_master_frame_count_cb(void)
{
    static uint8_t ax25_master_frame_counter = 0x00;

    return ax25_master_frame_counter++;
}

uint8_t tmtc_ax25_virtual_frame_count_cb(uint8_t virtual_channel_id)
{
    static uint8_t counters[8]; // 8 maximum virtual channels

    if (virtual_channel_id > 7)
        return 0;

    return counters[virtual_channel_id]++;
}

// Sending a telemetry every 4 seconds
void task1()
{
    int err;

    printf("\t task1: Starting ... \n\r");

    printf("\t task1: Starting I2C ... \n\r");
    err = I2C_start(I2C_BIT_RATE, I2C_TIMEOUT_RATE);
    if (err) {
        fprintf(stderr, "\t task1: Error from I2C_start: %d\n\r", err);
        for (;;) { /* HALT */ }
    }

    printf("\t task1: Initializing MIST TMTC API ... \n\r");

    err = tmtc_setup(NULL);
    if (err) {
        fprintf(stderr, "\t task1: Error Initializing tmtc_api: 0x%03x\n\r", err);
        for (;;) { /* HALT */ }
    }

    for (;;) {
        //Buffers and variable declarations
        size_t data_length = 4;
        uint8_t data_to_be_Sent[4];
        struct tmtc_telemetry tm;
        struct tmtc_tm_params params;

        data_to_be_Sent[0] = 'M';
        data_to_be_Sent[1] = 'I';
        data_to_be_Sent[2] = 'S';
        data_to_be_Sent[3] = 'T';

        params.virtual_channel_id = 0;
        params.apid = 12; //TMTC_APID_PL
        params.service_type = 176; //TMTC_ST_SEUD
        params.service_subtype = 13; //TMTC_SEUD_SST_RETRIEVE_STATUS
        params.time = 0x00000000;
        params.payload = data_to_be_Sent;
        params.payloadlen = data_length;

        err = tmtc_generate_tm(&tm, &params);
        if (err != 0) {
            fprintf(stderr, "\t task1: error from tmtc_generate_tm: 0x%03x\n\r", err);
            for (;;) { /* HALT */ }
        }

        // Retry 5 times
        for (int i = 0; i < 5; i++) {
            err = tmtc_send_tm(&tm);
            if (err == 0)
                break;

            fprintf(stderr, "\t task1: error from tmtc_send_tm: 0x%03x\n\r", err);

            vTaskDelay(1000);
        }

        if (err == 0)
            printf("\t task1: Sending TM OK ... \n\r");

        vTaskDelay(4000);
    }
}

// Retrieving telecommands from buffer every 2 seconds
void task2()
{
    int err;

    printf("\t task2: Starting ... \n\r");

    for (;;) {
        struct tmtc_telecommand tc;

        vTaskDelay(2000);

        err = tmtc_get_tc(&tc);

        if (err == 0) {
            printf("\t task2: Received TC: \n\r");
            printf("\t  - Sequence Count: %hu\n\r", tc.ccsds.sequence_count);
            printf("\t  - APID: %hu\n\r", tc.ccsds.apid);
            printf("\t  - Service Type: %u\n\r", tc.ccsds.service_type);
            printf("\t  - Service Subtype: %u\n\r", tc.ccsds.service_subtype);
        } else if (err != TMTC_ERR_RADIO_RX_BUFFER_IS_EMPTY) {
            fprintf(stderr, "\t task2: error from tmtc_get_tc: 0x%03x\n\r", err);
        }
    }
}

int main()
{
    TRACE_CONFIGURE_ISP(DBGU_STANDARD, 2000000, BOARD_MCK);

    // Enable the Instruction cache of the ARM9 core. Keep the MMU and Data
    // Cache disabled.
    CP15_Enable_I_Cache();

    printf("\n\r -- tmtc-api example program booted --\n\r");

    // The actual watchdog is already started, this only initializes the
    // watchdog-kick interface.
    WDT_start();

    //printf("\t main: Starting I2C ... \n\r");
    //int error = I2C_start(I2C_BIT_RATE, I2C_TIMEOUT_RATE);

    //if (error)
    //{
    //    fprintf(stderr, "\t main: Error in initializing I2C ... \n\r");
    //    for (;;)
    //    {
    //
    //    }
    //
    //}

    printf("\t main: Starting scheduler ... \n\r");

    xTaskHandle task1Handler;
    xTaskHandle task2Handler;

    xTaskCreate(task1, (const signed char* ) "task1", 1024, NULL,
            configMAX_PRIORITIES - 1, &task1Handler);
    xTaskCreate(task2, (const signed char* ) "task2", 1024, NULL,
            configMAX_PRIORITIES - 2, &task2Handler);

    vTaskStartScheduler();

    // Any code below this line should never be reached.

    return 0;
}
